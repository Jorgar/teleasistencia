﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################




'================================
'CLASE MODELO
'================================
'encargada de tratar todas las 
'conexiones y metodos que usa
'la base de datos MySQL
'================================

Imports MySql.Data.MySqlClient


Public Class Datos

    'Variables
    Private conexion As New MySqlConnection
    Private cadenaConexion As String = "server=localhost;database=teleasistencia;user id=admin;password=admin;"     'NOTA: Recomendable usar protocolo SSL
    Private Adaptador As New MySql.Data.MySqlClient.MySqlDataAdapter
    Private EnlaceDataTable As New BindingSource

    'Contructor
    Sub New()
        conexion = New MySqlConnection(cadenaConexion)  'creamos la conexion
    End Sub

    'Metodo get de Adapatador
    Public ReadOnly Property getAdaptador As MySql.Data.MySqlClient.MySqlDataAdapter
        Get
            Return Adaptador
        End Get
    End Property

    'Método get de enlaceDataTable
    Public ReadOnly Property getEnlaceDataTable As BindingSource
        Get
            Return EnlaceDataTable
        End Get
    End Property


    'Comprobación de la conexion con el servidor
    Sub comprobarConexion()
        Try
            Me.conexion.Open()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox("Se ha perdido la conexión con el servidor. Por favor, contacte con el servicio técnico.", MsgBoxStyle.Critical, "Sin conexión")
            Environment.Exit(0) 'Salimos del programa
        End Try
    End Sub

    'Devolvemos un dataTable con todos
    'los datos conseguidos a traves de la SQL
    Function devolverTabla(ByVal sentenciaSQL As String) As Data.DataTable
        Try
            Me.conexion.Open()
            Dim adaptador As New MySqlDataAdapter(sentenciaSQL, Me.conexion)
            Dim tabla As New Data.DataTable
            adaptador.Fill(tabla)
            conexion.Close()
            Return tabla
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
            Return Nothing
        End Try
    End Function

    'Cargamos los datos mediante una SQL 
    'en un dataTable y los volcamos en un 
    'objeto DataGridView
    Sub cargarDataGrid(ByVal sentenciaSQL As String, ByRef rejilla As DataGridView)
        Try
            Me.conexion.Open()
            Dim adaptador As New MySqlDataAdapter(sentenciaSQL, Me.conexion)
            Dim tabla As New Data.DataTable
            adaptador.Fill(tabla)
            conexion.Close()
            rejilla.DataSource = tabla
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    'Cargamos los datos mediante una SQL 
    'y los volcamos en un objeto Combobox
    Sub cargarComboBox(ByVal sentenciaSQL As String, ByRef combo As ComboBox)
        Try
            conexion.Open()
            Dim comando As New MySqlCommand(sentenciaSQL, conexion)
            comando.CommandType = CommandType.Text
            Dim datos As MySqlDataReader = comando.ExecuteReader()
            While datos.Read()
                Dim campo As String = datos.GetString(0)
                combo.Items.Add(campo)
            End While
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    'Cargamos los datos mediante una SQL 
    'y los volcamos en un objeto ListBox
    'cada campo cuenta con unos espacios
    'estéticos
    Sub cargarListBox(ByVal sentenciaSQL As String, ByRef list As ListBox, ByVal espacios As Boolean)
        Try
            conexion.Open()
            Dim comando As New MySqlCommand(sentenciaSQL, conexion)
            comando.CommandType = CommandType.Text
            Dim datos As MySqlDataReader = comando.ExecuteReader()
            While datos.Read()
                Dim campo As String = datos.GetString(0)
                If espacios Then
                    Dim espacio As String = "       "
                    list.Items.Add(espacio + campo)
                Else
                    list.Items.Add(campo)
                End If

            End While
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    'Creamos una nueva guardia en la 
    'base de datos
    Sub nuevaGuardia(ByRef fecha As String, ByRef dni As String, ByRef hora_ini As String, ByRef hora_fin As String)
        Try
            conexion.Open()

            Dim sql As String = "INSERT INTO guardias VALUES(@fecha,@dni,@hora_inicio,@hora_fin)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@fecha", fecha)
            comando.Parameters.AddWithValue("@dni", dni)
            comando.Parameters.AddWithValue("@hora_inicio", hora_ini)
            comando.Parameters.AddWithValue("@hora_fin", hora_fin)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    'Asignamos un medico a la asistencia
    'mediante una actualizacion en la bbdd
    Sub asignarMedicoAsistencia(ByRef n_medico As String, ByRef numero As Integer)
        Try
            conexion.Open()

            Dim sql As String = "UPDATE asistencias SET atiende=@n_medico WHERE numero=@numero"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@n_medico", n_medico)
            comando.Parameters.AddWithValue("@numero", numero)


            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevaAsistencia(ByRef fecha As String, ByRef hora As String, ByRef dni_cliente As String, ByRef tipo As Integer)
        Try
            conexion.Open()

            Dim sql As String = "INSERT INTO asistencias(fecha,hora,cliente,tipo) VALUES(@fecha,@hora,@dni_cliente,@tipo)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@fecha", fecha)
            comando.Parameters.AddWithValue("@hora", hora)
            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@tipo", tipo)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub borrarPatologiaPrincipal(ByRef dni_cliente As String)
        Try
            conexion.Open()

            Dim sql As String = "DELETE FROM patologias_clientes WHERE dni_cliente=@dni_cliente AND descripcion='PRINCIPAL'"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub insertarPatologiaPrincipal(ByRef dni_cliente As String, ByRef codigo As String)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO patologias_clientes VALUES(@dni_cliente,@codigo,'PRINCIPAL')"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@codigo", codigo)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub borrarTodasPatologiasSecundarias(ByRef dni_cliente As String)
        Try
            conexion.Open()

            Dim sql As String = "DELETE FROM patologias_clientes WHERE dni_cliente=@dni_cliente AND (descripcion<>'PRINCIPAL' OR descripcion IS NULL)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub insertarPatologiaSecundaria(ByRef dni_cliente As String, ByRef codigo As String)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO patologias_clientes(dni_cliente,patologia) VALUES(@dni_cliente,@codigo)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@codigo", codigo)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    'Devuelve true si el médico que se intenta asignar
    'a una guardia ya tiene una ese dia.
    Function comprobarGuardia(ByVal dni As String, ByVal fecha As String) As Boolean
        Dim yaGuardia As Boolean = False
        Try
            conexion.Open()
            Dim comando As New MySqlCommand("SELECT dni FROM guardias WHERE fecha='" + fecha + "'", conexion)
            comando.CommandType = CommandType.Text
            Dim datos As MySqlDataReader = comando.ExecuteReader()
            While datos.Read()
                Dim campo As String = datos.GetString(0)
                If (campo.Equals(dni)) Then
                    yaGuardia = True
                End If
            End While
            conexion.Close()
            Return yaGuardia
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
            Return Nothing
        End Try
    End Function

    Sub actualizarGuardia(ByVal dni As String, ByVal fecha As String, ByVal hora_ini As String, ByVal hora_fin As String)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE guardias SET hora_inicio=@hi , hora_fin=@hf WHERE dni=@dni AND fecha=@fecha"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@hi", hora_ini)
            comando.Parameters.AddWithValue("@hf", hora_fin)
            comando.Parameters.AddWithValue("@fecha", fecha)
            comando.Parameters.AddWithValue("@dni", dni)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub eliminarGuardia(ByVal dni As String, ByVal fecha As String)
        Try
            conexion.Open()
            Dim sql As String = "DELETE FROM guardias WHERE dni=@dni AND fecha=@fecha"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni", dni)
            comando.Parameters.AddWithValue("@fecha", fecha)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevaUrgencia(ByRef dni_cliente As String, ByRef fecha As String, ByRef hora As String)
        Try
            conexion.Open()

            Dim sql As String = "INSERT INTO urgencias(dni_cliente,fecha,hora) VALUES(@dni_cliente,@fecha,@hora)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@fecha", fecha)
            comando.Parameters.AddWithValue("@hora", hora)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarUrgencia(ByVal numero_urgencia As Integer, ByVal estado As Integer)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE urgencias SET estado=@estado WHERE numero=@numero_urgencia"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@estado", estado)
            comando.Parameters.AddWithValue("@numero_urgencia", numero_urgencia)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarAsistencia(ByVal numero_asistencia As Integer, ByVal estado As Integer, ByVal tipo As Integer)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE asistencias SET estado=@estado,tipo=@tipo WHERE numero=@numero_asistencia"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@estado", estado)
            comando.Parameters.AddWithValue("@numero_asistencia", numero_asistencia)
            comando.Parameters.AddWithValue("@tipo", tipo)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Sub actualizarAsistencia(ByVal numero_asistencia As Integer, ByVal hora_fin As String, ByVal obs As String, ByVal estado As Integer) 'otro tipo de actualización de la asistencia
        Try
            conexion.Open()
            Dim sql As String = "UPDATE asistencias SET finalizada=@hora_fin, observaciones=@obs, estado=@estado WHERE numero=@numero_asistencia"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@numero_asistencia", numero_asistencia)
            comando.Parameters.AddWithValue("@hora_fin", hora_fin)
            comando.Parameters.AddWithValue("@obs", obs)
            comando.Parameters.AddWithValue("@estado", estado)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarCliente(ByVal dni_paciente As String, ByVal dni_nuevo As String, ByVal apellidos As String, ByVal nombre As String,
                          ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono As Int64,
                          ByVal ss As Int64, ByVal fnac As Integer, ByVal sexo As String, ByVal cod_amb As Integer, ByVal cod_hos As Integer)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE clientes SET dni=@dni_nuevo, apellidos=@apellidos, nombre=@nombre, domicilio=@domicilio, poblacion=@poblacion,
                                cod_postal=@cpostal, telefono=@telefono, num_seg_social=@ss, fecha_nacimiento=@fnac,
                                sexo=@sexo, ambulatorio_referencia=@cod_amb, centro_hospitalario_referencia=@cod_hos WHERE dni=@dni_paciente"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_nuevo", dni_nuevo)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono", telefono)
            comando.Parameters.AddWithValue("@ss", ss)
            comando.Parameters.AddWithValue("@fnac", fnac)
            comando.Parameters.AddWithValue("@sexo", sexo)
            comando.Parameters.AddWithValue("@cod_amb", cod_amb)
            comando.Parameters.AddWithValue("@cod_hos", cod_hos)
            comando.Parameters.AddWithValue("@dni_paciente", dni_paciente)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevoCliente(ByVal dni_nuevo As String, ByVal apellidos As String, ByVal nombre As String,
                      ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono As Int64,
                      ByVal ss As Int64, ByVal fnac As Integer, ByVal sexo As String, ByVal cod_amb As Integer, ByVal cod_hos As Integer)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO clientes(dni,apellidos,nombre,domicilio,poblacion,cod_postal,telefono,num_seg_social,fecha_nacimiento,
                                    sexo,ambulatorio_referencia,centro_hospitalario_referencia) VALUES
                                (@dni_nuevo, @apellidos, @nombre, @domicilio, @poblacion,
                                @cpostal, @telefono, @ss, @fnac,@sexo, @cod_amb, @cod_hos)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_nuevo", dni_nuevo)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono", telefono)
            comando.Parameters.AddWithValue("@ss", ss)
            comando.Parameters.AddWithValue("@fnac", fnac)
            comando.Parameters.AddWithValue("@sexo", sexo)
            comando.Parameters.AddWithValue("@cod_amb", cod_amb)
            comando.Parameters.AddWithValue("@cod_hos", cod_hos)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub eliminar_cliente(ByRef dni As String)
        Try
            conexion.Open()
            Dim sql As String = "DELETE FROM clientes WHERE dni=@dni"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni", dni)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox("No se puede eliminar el paciente. Existen asistencias o urgencias ligadas.", MsgBoxStyle.Critical, "Error en la base de datos")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub eliminar_medico(ByRef dni As String)
        Try
            conexion.Open()
            Dim sql As String = "DELETE FROM medicos WHERE dni=@dni"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni", dni)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox("No se puede eliminar el médico. Existen asistencias o pacientes ligados.", MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub eliminar_operador(ByRef dni As String)
        Try
            conexion.Open()
            Dim sql As String = "DELETE FROM operadores WHERE dni=@dni"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni", dni)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarOperador(ByVal dni_operador As String, ByVal dni_nuevo As String, ByVal apellidos As String, ByVal nombre As String,
                          ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono1 As Int64,
                           ByVal telefono2 As Int64, ByVal login As String, ByVal password As String, ByVal email As String, ByVal nacceso As Integer)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE operadores SET dni=@dni_nuevo, apellidos=@apellidos, nombre=@nombre, domicilio=@domicilio, poblacion=@poblacion,
                                cod_postal=@cpostal, telefono1=@telefono1,telefono2=@telefono2, login=@login, password=@password,
                                email=@email, nivel_acceso=@nacceso WHERE dni=@dni_operador"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_nuevo", dni_nuevo)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono1", telefono1)
            comando.Parameters.AddWithValue("@telefono2", telefono2)
            comando.Parameters.AddWithValue("@login", login)
            comando.Parameters.AddWithValue("@password", password)
            comando.Parameters.AddWithValue("@email", email)
            comando.Parameters.AddWithValue("@nacceso", nacceso)
            comando.Parameters.AddWithValue("@dni_operador", dni_operador)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevoOperador(ByVal dni_operador As String, ByVal apellidos As String, ByVal nombre As String,
                          ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono1 As Int64,
                           ByVal telefono2 As Int64, ByVal login As String, ByVal password As String, ByVal email As String, ByVal nacceso As Integer)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO operadores VALUES (@dni_operador, @apellidos, @nombre, @domicilio, @poblacion,
                                @cpostal, @telefono1,@telefono2, @login, @password,@email, @nacceso)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_operador", dni_operador)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono1", telefono1)
            comando.Parameters.AddWithValue("@telefono2", telefono2)
            comando.Parameters.AddWithValue("@login", login)
            comando.Parameters.AddWithValue("@password", password)
            comando.Parameters.AddWithValue("@email", email)
            comando.Parameters.AddWithValue("@nacceso", nacceso)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub


    Sub eliminar_contacto(ByRef dni As String)
        Try
            conexion.Open()
            Dim sql As String = "DELETE FROM personas_contacto WHERE dni=@dni"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni", dni)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox("No se puede eliminar la persona de contacto. Existen pacientes ligados.", MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarContacto(ByVal dni_contacto As String, ByVal dni_contacto_nuevo As String, ByVal dni_cliente As String, ByVal apellidos As String, ByVal nombre As String,
                      ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono1 As Int64,
                      ByVal telefono2 As Int64, ByVal parentesco As String)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE personas_contacto SET dni=@dni_contacto_nuevo,dni_cliente=@dni_cliente, apellidos=@apellidos, nombre=@nombre, domicilio=@domicilio, poblacion=@poblacion,
                                cod_postal=@cpostal, telefono1=@telefono1, telefono2=@telefono2, parentesco=@parentesco WHERE dni=@dni_contacto"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_contacto_nuevo", dni_contacto_nuevo)
            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono1", telefono1)
            comando.Parameters.AddWithValue("@telefono2", telefono2)
            comando.Parameters.AddWithValue("@dni_contacto", dni_contacto)
            comando.Parameters.AddWithValue("@parentesco", parentesco)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub actualizarMedico(ByVal dni_medico As String, ByVal dni_medico_nuevo As String, ByVal apellidos As String, ByVal nombre As String,
                      ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono As Int64,
                      ByVal codEsp As Integer, ByVal codHosp As Integer, ByVal email As String)
        Try
            conexion.Open()
            Dim sql As String = "UPDATE medicos SET dni=@dni_medico_nuevo, apellidos=@apellidos, nombre=@nombre, domicilio=@domicilio, poblacion=@poblacion,
                                cod_postal=@cpostal, telefono=@telefono, especialidad=@codEsp, hospital=@codHosp, email=@email WHERE dni=@dni_medico"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_medico_nuevo", dni_medico_nuevo)
            comando.Parameters.AddWithValue("@dni_medico", dni_medico)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", domicilio)
            comando.Parameters.AddWithValue("@poblacion", poblacion)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono", telefono)
            comando.Parameters.AddWithValue("@codEsp", codEsp)
            comando.Parameters.AddWithValue("@codHosp", codHosp)
            comando.Parameters.AddWithValue("@email", email)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevoMedico(ByVal dni_medico As String, ByVal apellidos As String, ByVal nombre As String,
                      ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono As Int64,
                      ByVal codEsp As Integer, ByVal codHosp As Integer, ByVal email As String)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO medicos VALUES(@dni_medico, @apellidos, @nombre, @domicilio, @poblacion,
                                @cpostal, @telefono, @codEsp, @codHosp, @email)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_medico", dni_medico)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", domicilio)
            comando.Parameters.AddWithValue("@poblacion", poblacion)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono", telefono)
            comando.Parameters.AddWithValue("@codEsp", codEsp)
            comando.Parameters.AddWithValue("@codHosp", codHosp)
            comando.Parameters.AddWithValue("@email", email)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Sub nuevoContacto(ByVal dni_contacto As String, ByVal dni_cliente As String, ByVal apellidos As String, ByVal nombre As String,
                  ByVal domicilio As String, ByVal poblacion As String, ByVal cpostal As Integer, ByVal telefono1 As Int64,
                  ByVal telefono2 As Int64, ByVal parentesco As String)
        Try
            conexion.Open()
            Dim sql As String = "INSERT INTO personas_contacto VALUES(@dni_contacto,@dni_cliente, @apellidos, @nombre, @domicilio, @poblacion,
                                @cpostal, @telefono1, @telefono2, @parentesco)"
            Dim comando As New MySqlCommand(sql, conexion)
            comando.Prepare()

            comando.Parameters.AddWithValue("@dni_contacto", dni_contacto)
            comando.Parameters.AddWithValue("@dni_cliente", dni_cliente)
            comando.Parameters.AddWithValue("@apellidos", apellidos)
            comando.Parameters.AddWithValue("@nombre", nombre)
            comando.Parameters.AddWithValue("@domicilio", poblacion)
            comando.Parameters.AddWithValue("@poblacion", domicilio)
            comando.Parameters.AddWithValue("@cpostal", cpostal)
            comando.Parameters.AddWithValue("@telefono1", telefono1)
            comando.Parameters.AddWithValue("@telefono2", telefono2)
            comando.Parameters.AddWithValue("@parentesco", parentesco)

            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub


    Sub tablasDataGridView(ByRef datagrid As DataGridView, ByVal CadenaSql As String)
        Try
            conexion.Open()
            Dim ComandoActualizar As MySql.Data.MySqlClient.MySqlCommandBuilder
            ' Crear un nuevo adaptador de datos
            Adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(CadenaSql, conexion)
            ' Crear commandbuilder que genere el SQL Update /
            ' Insert / Delete no puede cambiarse de sitio pues
            ' captura datos de la operación anterior del adaptador.
            ComandoActualizar = New MySql.Data.MySqlClient.MySqlCommandBuilder(Adaptador)
            ' Llenar la tabla con los datos
            Dim Tabla As New DataTable
            Adaptador.Fill(Tabla)
            ' Enlazarla con el bindingsource
            datagrid.DataSource = EnlaceDataTable
            EnlaceDataTable.DataSource = Tabla
            conexion.Close()
        Catch ex As MySqlException
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Public Sub actualizarDataGrid()
        Try
            Adaptador.Update(CType(EnlaceDataTable.DataSource, DataTable))
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

End Class
