﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################







Imports System.ComponentModel

Module Informacion

    Public n_acceso As Integer  'nivel de seguridad de acceso que tiene el usuario
    Public login As String  'nombre de usuario o dni que ha iniciado sesion
    Public tema As Integer  'tema visual elegido por el usuario (claro u oscuro)

    'Nivel de acceso de los usuarios
    Public Enum nivel_acceso
        <Description("Sin acceso")> sin_acceso = 0
        <Description("Operador")> operador = 1
        <Description("Médico")> medico = 2
        <Description("Administrador")> admin = 3
    End Enum

    'Tipos de urgencias o asistencias
    Public Enum tipo_urgencia
        <Description("Sin selección")> sin_seleccion = 0
        <Description("Urgencia")> urgencia = 1
        <Description("Asistencia médica")> asistencia_medica = 2
        <Description("Asistencia periódica")> asistencia_periodica = 3
    End Enum

    'Estados de las urgencias y asistencias
    Public Enum estado_alarma
        <Description("Sin selección")> sin_seleccion = 0
        <Description("Abierta")> abierta = 1
        <Description("Cerrada")> cerrada = 2
        <Description("Derivada a centro hospitalario")> derivada_hosp = 3
        <Description("Derivada a familiar próximo")> derivada_fam = 4
    End Enum


    'Método necesario para devolver 
    'la descripcion de un item
    'en una enumeracion.
    Public Function getDescripcion(ByVal EnumConstant As [Enum]) As String
        Dim fi As Reflection.FieldInfo = EnumConstant.GetType().GetField(EnumConstant.ToString())
        Dim aattr() As DescriptionAttribute = DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
        If aattr.Length > 0 Then
            Return aattr(0).Description
        Else
            Return EnumConstant.ToString()
        End If
    End Function

End Module
