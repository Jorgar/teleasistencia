﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################






Imports System.Drawing.Printing

Public Class ListadosMedicos

    'Variables
    Private ContPag As Integer = 0
    Dim Imprimir As DialogResult
    Dim TextoCabecera() As CabecDetalle
    Dim contador As Integer = 0
    Dim eleccionListado As Integer '0 cod, 1 Alf, 2 c.sanit.

    Public Structure CabecDetalle
        Public Texto As String
        Public Cx As Long
    End Structure

    ' Estilos de impresión
    Public Est_Lin_Iden As Font = New Font("Arial", 6)
    Public Est_Titulo As Font = New Font("Times New Roman", 14, FontStyle.Italic)
    Public Est_Cabecera As Font = New Font("Arial", 12)
    Public Est_Lin_Det As Font = New Font("Arial", 10, FontStyle.Regular)
    Public Est_Lin_Det_Alb As Font = New Font("Arial", 10, FontStyle.Regular)
    Public Est_Pie As Font = New Font("Arial", 12)
    Public Est_Fin As Font = New Font("Arial", 12)



    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Dim bbdd As New Datos
    Private fecha_formateada As String
    Private dia, mes, anio As String

    Private Sub NuevaGuardia_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub NuevaGuardia_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub NuevaGuardia_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless



    Private Sub ListadosClientes_Load(sender As Object, e As EventArgs) Handles Me.Load
        ComboBox1.SelectedIndex = 0
    End Sub



    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Me.Close()
    End Sub
    'FIN CERRAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    'FIN MINIMIZAR


    '============================
    '
    '       LISTADOS
    '
    '============================



    'IMPRIMIR LISTADOS

    'CabecDetalle es estilo
    '


    Private Sub ConfigCabecera(ByRef Textocabecera() As CabecDetalle)
        Dim Fuente As Font
        Dim Lapiz As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Dim anchoString As New SizeF
        Dim Formato As New System.Drawing.StringFormat
        Dim Cx As Long
        Dim grafico As Graphics = Me.CreateGraphics
        Dim x As Integer
        Fuente = Est_Lin_Det
        'Campos del listado
        Select Case eleccionListado
            Case 0
                Textocabecera(0).Texto = "DNI       "
                Textocabecera(1).Texto = "Nombre                                   "
                Textocabecera(2).Texto = "Domicilio                      "
                Textocabecera(3).Texto = "Población                      "
                Textocabecera(4).Texto = "C.Postal    "
                Textocabecera(5).Texto = "Teléfono  "
            Case 1
                Textocabecera(0).Texto = "Nombre                                   "
                Textocabecera(1).Texto = "DNI       "
                Textocabecera(2).Texto = "Domicilio                      "
                Textocabecera(3).Texto = "Población                      "
                Textocabecera(4).Texto = "C.Posta    "
                Textocabecera(5).Texto = "Teléfono  "
            Case 2
                Textocabecera(0).Texto = "DNI       "
                Textocabecera(1).Texto = "Nombre                                   "
                Textocabecera(2).Texto = "Hospital       "
                Textocabecera(3).Texto = "Especialidad              "
        End Select

        'formato del texto 
        Formato.FormatFlags = StringFormatFlags.MeasureTrailingSpaces
        'margen lateral
        Cx = CLng(VistaPreviaDialogo.Size.Width * 0.1)
        'Fuente a utilizar
        Fuente = Est_Lin_Det
        'bucle de calculo
        While x < UBound(Textocabecera)
            Textocabecera(x).Cx = Cx
            anchoString = grafico.MeasureString(StrDup(Len(Textocabecera(x).Texto), "n"), Fuente)
            Cx = CLng(Cx + anchoString.Width)
            x = x + 1
        End While
    End Sub


    Public Sub LineaIden(ByRef Cy As Long, ByRef ContPag As Integer, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim fuente As Font
        Dim Pincel As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Dim Alto As Long = e.PageSettings.PaperSize.Height
        Dim Ancho As Long = e.PageSettings.PaperSize.Width
        Dim Cx As Long ' Coordenada horizontal Dim Texto As String 
        Dim Texto As String
        fuente = Est_Lin_Iden
        ContPag = ContPag + 1
        Cy = CLng(Alto * 0.02)
        Cx = CLng(Ancho * 0.05)
        Texto = "Médicos                                                  Pag: " & ContPag & "   " & Now
        e.Graphics.DrawString(Texto, fuente, Pincel, Cx, Cy)
        Cy = Cy + fuente.Height
    End Sub


    Public Sub Titulo(ByRef Cy As Long, ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal Texto As String)
        Dim fuente As Font
        Dim Pincel As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Dim Ancho As Long = e.PageSettings.PaperSize.Width
        Dim Cx As Long
        fuente = Est_Titulo
        Cx = CLng(Ancho * 0.05)
        Cy = Cy + fuente.Height
        e.Graphics.DrawString(Texto, fuente, Pincel, Cx, Cy)
        Cy = Cy + fuente.Height
    End Sub


    Public Sub Cabecera(ByRef Cy As Long, ByVal TextoCab() As CabecDetalle, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim Fuente As Font
        Dim Pincel As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Dim Lapiz As New Pen(Color.Black, 3)
        Dim Ancho As Long = e.PageSettings.PaperSize.Width
        Dim X As Integer
        Fuente = Est_Cabecera
        Cy = Cy + Fuente.Height ' Separacien del titulo
        While X < UBound(TextoCab)
            e.Graphics.DrawString(TextoCab(X).Texto, Fuente, Pincel, TextoCab(X).Cx, Cy)
            X = X + 1
        End While
        Cy = Cy + Fuente.Height ' Avance de Linea 
        Cy = Cy + 10 ' Linea de subrayado 

        If ComboBox1.SelectedIndex = 2 Then
            e.Graphics.DrawLine(Lapiz, CInt(Ancho * 0.05), Cy, CInt(Ancho * 0.95), Cy) 'VERTICAL  

        Else
            e.Graphics.DrawLine(Lapiz, CInt(Ancho * 0.05), Cy, CInt(Ancho * 1.36), Cy)   'HORIZONTAL. No se por qué se buguead y coge el ancho de vertical, por eso *1.36 a ojo
        End If

    End Sub


    Private Sub Cabeceras(ByRef Cy As Long, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        LineaIden(Cy, ContPag, e)
        Dim tituloTexto As String

        Select Case eleccionListado
            Case 0
                tituloTexto = "Listado de médicos por código"
            Case 1
                tituloTexto = "Listado de médicos por orden afabético"
            Case 2
                tituloTexto = "Listado de médicos por especialidad"
        End Select

        Titulo(Cy, e, tituloTexto)
        ConfigCabecera(TextoCabecera)
        Cabecera(Cy, TextoCabecera, e)
    End Sub


    Private Sub LineaDet(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByRef Cy As Long, ByVal fila As ArrayList)
        Dim Fuente As Font
        Dim Pincel As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Fuente = Est_Lin_Det

        Select Case eleccionListado
            Case 0
                e.Graphics.DrawString(fila.Item(0), Fuente, Pincel, TextoCabecera(0).Cx, Cy)
                e.Graphics.DrawString(fila.Item(1), Fuente, Pincel, TextoCabecera(1).Cx, Cy)
                e.Graphics.DrawString(fila.Item(2), Fuente, Pincel, TextoCabecera(2).Cx, Cy)
                e.Graphics.DrawString(fila.Item(3), Fuente, Pincel, TextoCabecera(3).Cx, Cy)
                e.Graphics.DrawString(fila.Item(4), Fuente, Pincel, TextoCabecera(4).Cx, Cy)
                e.Graphics.DrawString(fila.Item(5), Fuente, Pincel, TextoCabecera(5).Cx, Cy)
            Case 1
                e.Graphics.DrawString(fila.Item(0), Fuente, Pincel, TextoCabecera(0).Cx, Cy)
                e.Graphics.DrawString(fila.Item(1), Fuente, Pincel, TextoCabecera(1).Cx, Cy)
                e.Graphics.DrawString(fila.Item(2), Fuente, Pincel, TextoCabecera(2).Cx, Cy)
                e.Graphics.DrawString(fila.Item(3), Fuente, Pincel, TextoCabecera(3).Cx, Cy)
                e.Graphics.DrawString(fila.Item(4), Fuente, Pincel, TextoCabecera(4).Cx, Cy)
                e.Graphics.DrawString(fila.Item(5), Fuente, Pincel, TextoCabecera(5).Cx, Cy)
            Case 2
                e.Graphics.DrawString(fila.Item(0), Fuente, Pincel, TextoCabecera(0).Cx, Cy)
                e.Graphics.DrawString(fila.Item(1), Fuente, Pincel, TextoCabecera(1).Cx, Cy)
                e.Graphics.DrawString(fila.Item(2), Fuente, Pincel, TextoCabecera(2).Cx, Cy)
                e.Graphics.DrawString(fila.Item(3), Fuente, Pincel, TextoCabecera(3).Cx, Cy)
        End Select

        Cy = Cy + Fuente.Height
    End Sub


    Public Sub PiePagina(ByVal Cy As Long, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim Alto As Long = e.PageSettings.PaperSize.Height
        Dim Ancho As Long = e.PageSettings.PaperSize.Width
        Dim Fuente As Font
        Dim Pincel As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
        Dim Cx As Long
        Dim Texto As String
        Fuente = Est_Pie

        If ComboBox1.SelectedIndex = 2 Then
            Cy = CLng(Alto * 0.95)
        Else
            Cy = CLng(Alto * 0.65) ' Situacion con respecto al alto de la pagina 0.65 por que esta bugueado. 
        End If

        Cx = CLng(Ancho * 0.05) ' Situacion con respecto al ancho de la pagina 
        Texto = "Pag: " + CType(ContPag, String)
        e.Graphics.DrawString(Texto, Fuente, Pincel, Cx, Cy)
        Cy = Cy + Fuente.Height
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        eleccionListado = ComboBox1.SelectedIndex

        Select Case eleccionListado
            Case 0
                ReDim TextoCabecera(6)
            Case 1
                ReDim TextoCabecera(6)
            Case 2
                ReDim TextoCabecera(4)
        End Select


        ' Definimos las características
        PageSetupDialog1.AllowMargins = True
        PageSetupDialog1.AllowOrientation = False
        PageSetupDialog1.AllowPaper = False
        PageSetupDialog1.AllowPrinter = True
        PageSetupDialog1.Document = Hoja
        ' Captura del resultado.
        PageSetupDialog1.PageSettings = New System.Drawing.Printing.PageSettings

        If ComboBox1.SelectedIndex = 2 Then
            PageSetupDialog1.PageSettings.Landscape = False
        Else
            PageSetupDialog1.PageSettings.Landscape = True
        End If



        If PageSetupDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Imprimir = DialogResult.OK
        End If

        ' Inicio del listado
        Select Case Imprimir
            Case DialogResult.OK
                Try
                    VistaPreviaDialogo.Document = Hoja
                    VistaPreviaDialogo.Show()
                Catch ex As Exception
                    Debug.Print(ex.ToString)
                End Try
        End Select
    End Sub

    Private Sub Hoja_PrintPage(sender As Object, e As PrintPageEventArgs) Handles Hoja.PrintPage
        Dim Cy As Long
        Dim Cabec As Boolean = True
        Dim pie As Boolean = False
        Dim tablaDatos As DataTable

        Select Case eleccionListado
            Case 0

                If (TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals("")) And
                    (TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals("")) Then
                    tablaDatos = bbdd.devolverTabla("SELECT dni, CONCAT(apellidos,', ',nombre), domicilio,
                                                     poblacion, cod_postal, telefono FROM medicos ORDER BY dni")
                ElseIf ((TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT dni, CONCAT(apellidos,', ',nombre), domicilio,
                                                    poblacion, cod_postal, telefono FROM medicos WHERE CAST(SUBSTRING(dni,1,1)
                                                    AS UNSIGNED) BETWEEN " + TextBox1.Text.Trim.Substring(0, 1) + " AND 9 ORDER BY dni")
                ElseIf ((TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT dni, CONCAT(apellidos,', ',nombre), domicilio, poblacion, cod_postal, 
                                                     telefono FROM medicos WHERE CAST(SUBSTRING(dni,1,1) AS UNSIGNED) BETWEEN 0 
                                                     AND " + TextBox2.Text.Trim.Substring(0, 1) + " ORDER BY dni")
                Else
                    tablaDatos = bbdd.devolverTabla("SELECT dni, CONCAT(apellidos,', ',nombre), domicilio, poblacion, cod_postal,
                                                    telefono FROM medicos WHERE CAST(SUBSTRING(dni,1,1) AS UNSIGNED) BETWEEN " _
                                                    + TextBox1.Text.Trim.Substring(0, 1) + " AND " + TextBox2.Text.Trim.Substring(0, 1) + " ORDER BY dni")
                End If
            Case 1

                If (TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals("")) And
                   (TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals("")) Then
                    tablaDatos = bbdd.devolverTabla("SELECT CONCAT(apellidos,', ',nombre), dni, domicilio, poblacion, cod_postal,
                                                    telefono FROM medicos ORDER BY apellidos")
                ElseIf ((TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT CONCAT(apellidos,', ',nombre), dni, domicilio, poblacion, cod_postal,
                                                    telefono FROM medicos WHERE SUBSTRING(apellidos,1,1) BETWEEN '" _
                                                    + TextBox1.Text.Trim.Substring(0, 1) + "' AND 'z' ORDER BY apellidos")
                ElseIf ((TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT CONCAT(apellidos,', ',nombre), dni, domicilio, poblacion, cod_postal,
                                                    telefono FROM medicos WHERE SUBSTRING(apellidos,1,1) BETWEEN 'a' AND
                                                    '" + TextBox2.Text.Trim.Substring(0, 1) + "' ORDER BY apellidos")
                Else
                    tablaDatos = bbdd.devolverTabla("SELECT CONCAT(apellidos,', ',nombre), dni, domicilio, poblacion, cod_postal,
                                                    telefono FROM medicos WHERE SUBSTRING(apellidos,1,1) BETWEEN
                                                    '" + TextBox1.Text.Trim.Substring(0, 1) + "' AND '" + TextBox2.Text.Trim.Substring(0, 1) + "' ORDER BY apellidos")
                End If

            Case 2

                If (TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals("")) And
                    (TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals("")) Then
                    tablaDatos = bbdd.devolverTabla("SELECT medicos.dni, CONCAT(medicos.apellidos,', ',medicos.nombre),
                                                    medicos.hospital, especialidades.nombre FROM medicos INNER JOIN especialidades
                                                    ON medicos.especialidad = especialidades.codigo ORDER BY medicos.dni")
                ElseIf ((TextBox2.Text Is Nothing Or TextBox2.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT medicos.dni, CONCAT(medicos.apellidos,', ',medicos.nombre), 
                                                    medicos.hospital, especialidades.nombre FROM medicos INNER JOIN 
                                                    especialidades ON medicos.especialidad = especialidades.codigo AND (CAST(SUBSTRING(dni,1,1) AS UNSIGNED) BETWEEN 9 AND " + TextBox2.Text.Trim.Substring(0, 1) + ") ORDER BY medicos.dni")
                ElseIf ((TextBox1.Text Is Nothing Or TextBox1.Text.Trim.Equals(""))) Then
                    tablaDatos = bbdd.devolverTabla("SELECT medicos.dni, CONCAT(medicos.apellidos,', ',medicos.nombre), 
                                                    medicos.hospital, especialidades.nombre FROM medicos INNER JOIN 
                                                    especialidades ON medicos.especialidad = especialidades.codigo AND 
                                                    (CAST(SUBSTRING(dni,1,1) AS UNSIGNED) BETWEEN 0 AND 
                                                    " + TextBox2.Text.Trim.Substring(0, 1) + ") ORDER BY medicos.dni")
                Else
                    tablaDatos = bbdd.devolverTabla("SELECT medicos.dni, CONCAT(medicos.apellidos,', ',medicos.nombre),
                                                    medicos.hospital, especialidades.nombre FROM medicos INNER JOIN
                                                    especialidades ON medicos.especialidad = especialidades.codigo AND 
                                                    (CAST(SUBSTRING(dni,1,1) AS UNSIGNED) BETWEEN
                                                    " + TextBox1.Text.Trim.Substring(0, 1) + " AND 
                                                    " + TextBox2.Text.Trim.Substring(0, 1) + ") ORDER BY medicos.dni")
                End If



        End Select

        Dim fila As New ArrayList

        Do While Me.contador < tablaDatos.Rows.Count
            If pie Then
                PiePagina(Cy, e)
                e.HasMorePages = True
                Exit Sub
            End If

            If Cabec Then
                Cabeceras(Cy, e)
                Cabec = False
            End If

            fila.Clear()
            For c As Integer = 0 To tablaDatos.Columns.Count - 1
                Debug.Print(tablaDatos.Rows(contador).Item(c).ToString)
                fila.Add(tablaDatos.Rows(contador).Item(c).ToString)
            Next

            LineaDet(e, Cy, fila) ' Control de finde pagina 
            pie = Cy > e.MarginBounds.Height

            contador += 1
        Loop

        PiePagina(Cy, e)
        e.HasMorePages = False
        Me.Close()
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

        eleccionListado = ComboBox1.SelectedIndex
        TextBox1.Text = ""
        TextBox2.Text = ""

        Select Case eleccionListado
            Case 0
                TextBox1.MaxLength = 8
                TextBox2.MaxLength = 8
                RemoveHandler TextBox1.KeyPress, AddressOf filtrado_soloLetras
                RemoveHandler TextBox2.KeyPress, AddressOf filtrado_soloLetras
                AddHandler TextBox1.KeyPress, AddressOf filtrado_numeros
                AddHandler TextBox2.KeyPress, AddressOf filtrado_numeros
            Case 1
                TextBox1.MaxLength = 25
                TextBox2.MaxLength = 25
                RemoveHandler TextBox1.KeyPress, AddressOf filtrado_numeros
                RemoveHandler TextBox2.KeyPress, AddressOf filtrado_numeros
                AddHandler TextBox1.KeyPress, AddressOf filtrado_soloLetras
                AddHandler TextBox2.KeyPress, AddressOf filtrado_soloLetras
            Case 2
                TextBox1.MaxLength = 8
                TextBox2.MaxLength = 8
                RemoveHandler TextBox1.KeyPress, AddressOf filtrado_soloLetras
                RemoveHandler TextBox2.KeyPress, AddressOf filtrado_soloLetras
                AddHandler TextBox1.KeyPress, AddressOf filtrado_numeros
                AddHandler TextBox2.KeyPress, AddressOf filtrado_numeros
        End Select
    End Sub

    Private Sub filtrado_numeros(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Select Case e.KeyChar.IsNumber(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Chr(Keys.Space)
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

    Private Sub filtrado_soloLetras(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Select Case e.KeyChar.IsLetter(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Chr(Keys.Space)
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub



End Class