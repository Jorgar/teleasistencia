﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################


Public Class ContenidoPersonasDeContacto

    'Variables
    Dim bbdd As New Datos()
    Dim esEdicion As Boolean
    Private fecha_formateada As String
    Private dia, mes, anio As String
    Private nombre, apellidos As String

    Private Sub ContenidoPacientes_Load(sender As Object, e As EventArgs) Handles Me.Load
        bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM personas_contacto", ListBox1, True)
        bbdd.cargarComboBox("SELECT dni FROM clientes ORDER BY dni", ComboBox4)
        bloquear()
        esEdicion = False
        ListBox1.SelectedIndex = 0
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        Dim array() As String = ListBox1.SelectedItem.ToString.Split(",")
        apellidos = array(0).Trim
        nombre = array(1).Trim
        If Not esEdicion Then
            cargar_datos_contacto(nombre, apellidos)
        Else
            bloquear()
            cargar_datos_contacto(nombre, apellidos)
        End If
    End Sub

    Private Sub PictureBox1_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox1.MouseEnter
        PictureBox1.Image = My.Resources.buscar_over
    End Sub

    Private Sub PictureBox1_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox1.MouseLeave
        PictureBox1.Image = My.Resources.buscar_normal
    End Sub

    Private Sub PictureBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseDown
        PictureBox1.Image = My.Resources.buscar_press
    End Sub

    Private Sub PictureBox1_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseUp
        PictureBox1.Image = My.Resources.buscar_normal
    End Sub


    'Edicion
    Private Sub PictureBox3_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox2.MouseEnter
        PictureBox2.Image = My.Resources.edit_over
    End Sub

    Private Sub PictureBox3_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox2.MouseLeave
        PictureBox2.Image = My.Resources.edit_normal
    End Sub

    Private Sub PictureBox3_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseDown
        PictureBox2.Image = My.Resources.edit_press
    End Sub

    Private Sub PictureBox3_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseUp
        PictureBox2.Image = My.Resources.edit_normal
    End Sub

    'Eliminar
    Private Sub PictureBox4_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox4.MouseEnter
        PictureBox4.Image = My.Resources.eliminar_over
    End Sub

    Private Sub PictureBox4_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox4.MouseLeave
        PictureBox4.Image = My.Resources.eliminar_normal
    End Sub

    Private Sub PictureBox4_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox4.MouseDown
        PictureBox4.Image = My.Resources.eliminar_press
    End Sub

    Private Sub PictureBox4_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox4.MouseUp
        PictureBox4.Image = My.Resources.eliminar_normal
    End Sub




    Sub cargar_datos_contacto(ByVal nombre As String, ByVal apellidos As String)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT * FROM personas_contacto WHERE nombre ='" + nombre + "' AND
                                            apellidos='" + apellidos + "'")

        For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos
            Select Case i
                Case 0  'dni
                    TextBox9.Text = tabla_datos.Rows(0)(i).ToString
                Case 1  'dni_cliente
                    ComboBox4.SelectedItem = tabla_datos.Rows(0)(i).ToString
                Case 2  'apellidos
                    TextBox4.Text = tabla_datos.Rows(0)(i).ToString
                Case 3  'nombre
                    TextBox2.Text = tabla_datos.Rows(0)(i).ToString
                Case 4  'domicilio
                    TextBox7.Text = tabla_datos.Rows(0)(i).ToString
                Case 5  'poblacion
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString
                Case 6  'cod_postal
                    TextBox3.Text = tabla_datos.Rows(0)(i).ToString
                Case 7  'telefono1
                    TextBox8.Text = tabla_datos.Rows(0)(i).ToString
                Case 8  'telefono2
                    TextBox10.Text = tabla_datos.Rows(0)(i).ToString
                Case 9  'Parentesco
                    TextBox11.Text = tabla_datos.Rows(0)(i).ToString
            End Select
        Next
    End Sub

    Private Sub bloquear()  'bloquea los controles para prevenir su edición
        'submenu de edición
        Button4.Visible = False
        Button5.Visible = False
        Button6.Visible = False

        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                subobjeto.BackColor = Color.FromArgb(248, 248, 248)             'cambiamos el color del panel a gris
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            campo.BackColor = Color.FromArgb(248, 248, 248)      'cambiamos el color del textbox a gris
                                            campo.ReadOnly = True                                'ponemos el control solo en modo lectura
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            campo.Enabled = False
                                            campo.FlatStyle = FlatStyle.Standard                                 'Desabilitamos los combobox
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
    End Sub

    Private Sub desbloquear()   'desbloquea los controles para su edición
        'submenu de edición
        Button4.Visible = True
        Button5.Visible = True
        Button6.Visible = True

        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                subobjeto.BackColor = Color.White             'cambiamos el color del panel a blanco
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            If Not campo.Name.Contains("TextBox9") Then
                                                campo.BackColor = Color.White      'cambiamos el color del textbox a blanco
                                                campo.ReadOnly = False             'ponemos el control solo en modo escritura
                                            End If
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            campo.FlatStyle = FlatStyle.Flat        'Ponemos el stilo del combobox a su defecto
                                            campo.DropDownStyle = ComboBoxStyle.DropDown
                                            campo.DropDownStyle = ComboBoxStyle.DropDownList
                                            campo.Enabled = True                    'Habilitamos los combobox
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
    End Sub

    Private Sub borrar_campos()   'borra el contenido de los controles
        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            campo.Text = ""
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            campo.SelectedItem = Nothing
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Not validarCampos() Then
            MsgBox("Rellene los campos vacíos", MsgBoxStyle.Critical, "Error en los campos")
            Exit Sub
        End If

        actualizar()

        'recargamos en panel4 del form Principal para que se actualizen los datos visualmente
        Dim contenido_personasdecontacto As New ContenidoPersonasDeContacto()
        Principal.Panel4.Controls.Clear()
        contenido_personasdecontacto.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
        contenido_personasdecontacto.TopLevel = False
        Principal.Panel4.Controls.Add(contenido_personasdecontacto)
        Principal.contenido_personasdecontacto = contenido_personasdecontacto
        contenido_personasdecontacto.Show()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea cancelar la edición del contacto?", MsgBoxStyle.YesNo, "Cancelar edición")
        If comprobacion = 6 Then
            Button4.Visible = False
            Button5.Visible = False
            Button6.Visible = False
            cargar_datos_contacto(nombre, apellidos)
            bloquear()
        End If
    End Sub

    Private Sub formatear_fecha(ByRef dia As String, ByRef mes As String, ByRef anio As String)
        If (dia.Length < 2) Then
            dia = "0" + dia
        End If
        If (mes.Length < 2) Then
            mes = "0" + mes
        End If
        fecha_formateada = anio + mes + dia
    End Sub

    Private Sub actualizar()
        Try
            Button4.Visible = False
            Button5.Visible = False
            Button6.Visible = False

            Dim dni As String = TextBox9.Text
            Dim dni_cliente As String = ComboBox4.Text
            Dim apellidos As String = TextBox4.Text
            Dim nombre As String = TextBox2.Text
            Dim domicilio As String = TextBox7.Text
            Dim poblacion As String = TextBox6.Text
            Dim cod_postal As String = TextBox3.Text
            Dim telefono1 As Int64 = CType(TextBox8.Text, Int64)
            Dim telefono2 As Int64 = CType(TextBox10.Text, Int64)
            Dim parentesco As String = TextBox11.Text

            Try
                bbdd.actualizarContacto(TextBox9.Text.Trim, dni, dni_cliente, apellidos, nombre, domicilio, poblacion, cod_postal,
                           telefono1, telefono2, parentesco)
                MsgBox("Datos guardado satisfactoriamente.", MsgBoxStyle.DefaultButton1, "Actualización")
            Catch ex As Exception
                MsgBox("Error al actualizar datos.", MsgBoxStyle.Critical, "Error en actualización")
            End Try
            bloquear()
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea borrar todos los campos?", MsgBoxStyle.YesNo, "Borrar campos")
        If comprobacion = 6 Then
            borrar_campos()
        End If
    End Sub

    Private Sub Filtrado_Numeros_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox8.KeyPress, TextBox10.KeyPress, TextBox3.KeyPress
        Select Case e.KeyChar.IsNumber(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

    Private Sub Filtrado_Letras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress, TextBox2.KeyPress
        Select Case e.KeyChar.IsLetter(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Chr(Keys.Space)
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

    Private Sub ContenidoPacientes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        Select Case e.KeyChar = ControlChars.Cr
            Case True
                System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
        End Select
    End Sub




    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.Click
        TextBox5.Text = ""
        TextBox5.ForeColor = Color.DimGray
    End Sub


    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If Not TextBox5.Text.Equals("") Then
            ListBox1.Items.Clear()
            bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM personas_contacto WHERE dni='" + TextBox5.Text + "' OR dni_cliente='" + TextBox5.Text + "' OR  apellidos='" + TextBox5.Text +
                "' OR  nombre='" + TextBox5.Text + "'", ListBox1, True)
        Else
            ListBox1.Items.Clear()
            bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM personas_contacto", ListBox1, True)
        End If

    End Sub



    '********
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim ventana_nuevo_contacto As New nuevoContacto
        ventana_nuevo_contacto.ShowDialog()
    End Sub

    Function comprobarDNI(ByVal dni As String) As Boolean
        Dim existe = False
        If (bbdd.devolverTabla("SELECT * FROM personas_contacto WHERE dni='" + dni + "'").Rows.Count > 0 And Not dni.Equals(ListBox1.SelectedItem.ToString.Trim)) Then
            existe = True
        End If
        Return existe
    End Function

    Function validarCampos() As Boolean
        Dim validado = True
        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            If campo.Text = Nothing Then
                                                campo.BackColor = Color.SeaShell
                                                subobjeto.BackColor = Color.SeaShell
                                                validado = False
                                            Else
                                                campo.BackColor = Color.White
                                                subobjeto.BackColor = Color.White
                                            End If
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            If campo.SelectedItem = Nothing Then
                                                campo.BackColor = Color.SeaShell
                                                subobjeto.BackColor = Color.SeaShell
                                                validado = False
                                            Else
                                                campo.BackColor = Color.White
                                                subobjeto.BackColor = Color.White
                                            End If
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
        Return validado
    End Function


    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        esEdicion = True
        desbloquear()
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea eliminar el contacto " + ListBox1.SelectedItem.ToString.Trim + " ?", MsgBoxStyle.YesNo, "Eliminar paciente")
        If comprobacion = 6 Then
            bbdd.eliminar_contacto(TextBox9.Text.Trim)
            'recargamos en panel4 del form Principal para que se actualizen los datos visualmente
            Dim contenido_personasdecontacto As New ContenidoPersonasDeContacto()
            Principal.Panel4.Controls.Clear()
            contenido_personasdecontacto.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
            contenido_personasdecontacto.TopLevel = False
            Principal.Panel4.Controls.Add(contenido_personasdecontacto)
            Principal.contenido_personasdecontacto = contenido_personasdecontacto
            contenido_personasdecontacto.Show()
        End If
    End Sub

End Class