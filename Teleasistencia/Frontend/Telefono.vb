﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################


Public Class Telefono

    'variables
    Private maximizado As Boolean = False
    Private numeroTelf As String
    Private tipo As Integer
    Private bbdd As New Datos
    Private fechaHoy As DateTime = DateTime.Today
    Private diaHoy As String = fechaHoy.Day.ToString
    Private mesHoy As String = fechaHoy.Month.ToString
    Private añoHoy As String = fechaHoy.Year.ToString
    Private horaHoy As String = DateTime.Now.Hour.ToString
    Private minutosHoy As String = DateTime.Now.Minute.ToString

    Private Sub Llamada_Load(sender As Object, e As EventArgs) Handles Me.Load
        Label1.Text = ""
        ComboBox1.SelectedIndex = 0
    End Sub

    Private Sub ButtosnsClick(sender As Object, e As EventArgs) Handles Button1.Click, Button2.Click, Button3.Click,
                                                                        Button4.Click, Button5.Click, Button6.Click,
                                                                        Button7.Click, Button8.Click, Button9.Click,
                                                                        Button0.Click
        Dim cual As Integer = CInt(Microsoft.VisualBasic.Strings.Right(CType(sender, Button).Name, 1))
        Select Case cual
            Case 0
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "0"
                End If
            Case 1
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "1"
                End If
            Case 2
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "2"
                End If
            Case 3
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "3"
                End If
            Case 4
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "4"
                End If
            Case 5
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "5"
                End If
            Case 6
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "6"
                End If
            Case 7
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "7"
                End If
            Case 8
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "8"
                End If
            Case 9
                If (Label1.Text.Length < 11) Then
                    If (Label1.Text.Length = 3 Or Label1.Text.Length = 7) Then
                        Label1.Text += " "
                    End If
                    Label1.Text += "9"
                End If
        End Select

    End Sub


    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        numeroTelf = Label1.Text.Replace(" ", "")
        Debug.Print("" & numeroTelf)

        Dim dni_cliente As String
        Try
            dni_cliente = bbdd.devolverTabla("SELECT dni FROM clientes WHERE telefono = '" + numeroTelf + "'").Rows(0)(0).ToString

            Select Case ComboBox1.SelectedIndex
                Case 0 'Urgencia
                    'Actualizar las variables a la hora del sistema en el instante del click 
                    fechaHoy = DateTime.Today
                    diaHoy = fechaHoy.Day.ToString
                    mesHoy = fechaHoy.Month.ToString
                    añoHoy = fechaHoy.Year.ToString
                    horaHoy = DateTime.Now.Hour.ToString
                    minutosHoy = DateTime.Now.Minute.ToString

                    bbdd.nuevaUrgencia(dni_cliente, formatear_fecha(diaHoy, mesHoy, añoHoy), horaHoy + ":" + minutosHoy)

                    'recargamos en panel4 del form Principal para que se actualizen los datso visualmente
                    Dim contenido_llamada_urgencia As New ContenidoLLUrgencia()
                    Principal.Panel4.Controls.Clear()
                    contenido_llamada_urgencia.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
                    contenido_llamada_urgencia.TopLevel = False
                    Principal.Panel4.Controls.Add(contenido_llamada_urgencia)
                    Principal.contenido_llamada_urgencia = contenido_llamada_urgencia
                    contenido_llamada_urgencia.Show()

                    'Recargamos el panel3 de Principal con el menu correspondiente
                    Dim menu_llamadas As New MenuLlamadas()
                    Principal.Panel3.Controls.Clear()
                    menu_llamadas.TopLevel = False
                    Principal.Panel3.Controls.Add(menu_llamadas)
                    menu_llamadas.Show()
                    menu_llamadas.reset()

                    'Seleccionamos el boton Llamadas del menu principal
                    For Each b As Button In Principal.Panel2.Controls.OfType(Of Button)
                        If b.Enabled = True Then
                            If Informacion.tema = 0 Then
                                b.BackColor = Color.FromArgb(0, 174, 239)
                                b.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 184, 249)
                            ElseIf Informacion.tema = 1 Then
                                b.BackColor = Color.FromArgb(205, 205, 205)
                                b.FlatAppearance.MouseOverBackColor = Color.FromArgb(219, 219, 219)
                                b.ForeColor = Color.Gray
                            End If
                        End If
                    Next

                    If Informacion.tema = 0 Then
                        Principal.Button1.BackColor = Color.FromArgb(0, 144, 197)
                        Principal.Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)

                    ElseIf Informacion.tema = 1 Then
                        Principal.Button1.BackColor = Color.FromArgb(103, 103, 103)
                        Principal.Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
                        Principal.Button1.ForeColor = Color.White
                    End If

                Case 1, 2 'Asistencia médica
                    'Actualizar las variables a la hora del sistema en el instante del click 
                    fechaHoy = DateTime.Today
                    diaHoy = fechaHoy.Day.ToString
                    mesHoy = fechaHoy.Month.ToString
                    añoHoy = fechaHoy.Year.ToString
                    horaHoy = DateTime.Now.Hour.ToString
                    minutosHoy = DateTime.Now.Minute.ToString

                    bbdd.nuevaAsistencia(formatear_fecha(diaHoy, mesHoy, añoHoy), horaHoy + ":" + minutosHoy, dni_cliente, ComboBox1.SelectedIndex)

                    'recargamos en panel4 del form Principal para que se actualizen los datso visualmente
                    Dim contenido_llamada_asistencia As New ContenidoLLAsistencia()
                    Principal.Panel4.Controls.Clear()
                    contenido_llamada_asistencia.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
                    contenido_llamada_asistencia.TopLevel = False
                    Principal.Panel4.Controls.Add(contenido_llamada_asistencia)
                    Principal.contenido_llamada_asistencia = contenido_llamada_asistencia
                    contenido_llamada_asistencia.Show()

                    'Recargamos el panel3 de Principal con el menu correspondiente
                    Dim menu_llamadas As New MenuLlamadas()
                    Principal.Panel3.Controls.Clear()
                    menu_llamadas.TopLevel = False
                    Principal.Panel3.Controls.Add(menu_llamadas)
                    menu_llamadas.Show()
                    menu_llamadas.reset()
                    menu_llamadas.PanelSelect.Size = New Size(menu_llamadas.Label3.Size.Width, menu_llamadas.PanelSelect.Size.Height)
                    menu_llamadas.PanelSelect.Location = New Point(menu_llamadas.Label3.Location.X, menu_llamadas.PanelSelect.Location.Y)

                    'Seleccionamos el boton Llamadas del menu principal
                    For Each b As Button In Principal.Panel2.Controls.OfType(Of Button)
                        If b.Enabled = True Then
                            If Informacion.tema = 0 Then
                                b.BackColor = Color.FromArgb(0, 174, 239)
                                b.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 184, 249)
                            ElseIf Informacion.tema = 1 Then
                                b.BackColor = Color.FromArgb(205, 205, 205)
                                b.FlatAppearance.MouseOverBackColor = Color.FromArgb(219, 219, 219)
                                b.ForeColor = Color.Gray
                            End If
                        End If
                    Next

                    If Informacion.tema = 0 Then
                        Principal.Button1.BackColor = Color.FromArgb(0, 144, 197)
                        Principal.Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)

                    ElseIf Informacion.tema = 1 Then
                        Principal.Button1.BackColor = Color.FromArgb(103, 103, 103)
                        Principal.Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
                        Principal.Button1.ForeColor = Color.White
                    End If

            End Select
        Catch ex As Exception
            MsgBox("Número de teléfono desconocido", MsgBoxStyle.Exclamation, "Error al llamar")
        End Try




    End Sub


    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Me.Close()
    End Sub
    'FIN CERRAR

    'INICIO MAXIMIZAR
    Private Sub PictureBox6_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.Image = My.Resources.maximizar_over
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseUp
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseDown
        PictureBox6.Image = My.Resources.maximizar_pulsado
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        If Not (maximizado) Then
            Me.MaximumSize = Screen.FromRectangle(Me.Bounds).WorkingArea.Size 'para que no se maximize encima de la barra de menu de windows
            Me.WindowState = FormWindowState.Maximized
            maximizado = True
        Else
            Me.WindowState = FormWindowState.Normal
            'Me.Size = New Size(1125, 756)
            maximizado = False
        End If
    End Sub
    'FIN MAXIMIZAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub



    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer

    Private Sub Login_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown, Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub Login_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp, Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub Login_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove, Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        If (Label1.Text.Length > 0) Then
            If (Label1.Text.Substring(Label1.Text.Length - 1).Equals(" ")) Then
                Label1.Text = Label1.Text.Remove(Label1.Text.Length - 1, 1)
            End If
            Label1.Text = Label1.Text.Remove(Label1.Text.Length - 1, 1)
        End If
    End Sub
    '/Mover ventana form borderless

    Private Function formatear_fecha(ByRef dia As String, ByRef mes As String, ByRef anio As String) As String
        Dim fecha_formateada As String
        If (dia.Length < 2) Then
            dia = "0" + dia
        End If
        If (mes.Length < 2) Then
            mes = "0" + mes
        End If
        fecha_formateada = anio + mes + dia
        Return fecha_formateada
    End Function

End Class