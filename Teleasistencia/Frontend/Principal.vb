﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################


Public Class Principal

    'Variables de clase
    Private maximizado As Boolean = False
    Public autentificado As Boolean

    'Variables de menu
    Dim menu_llamadas As New MenuLlamadas()
    Dim menu_medicosProv As New MenuMedicos()
    Dim menu_clientes As New MenuClientes()
    Dim menu_plantilla As New MenuPlantilla()
    Dim menu_tablas As New MenuTablas()

    'Variables de contenido
    Public contenido_llamada_urgencia As New ContenidoLLUrgencia()
    Public contenido_llamada_asistencia As New ContenidoLLAsistencia()
    Public contenido_asistencias As New ContenidoAsistencias()
    Public contenido_guardias As New ContenidoGuardias()
    Public contenido_pacientes As New ContenidoPacientes()
    Public contenido_personasdecontacto As New ContenidoPersonasDeContacto()
    Public contenido_patologiaclientes As New ContenidoPatologiaDeClientes()
    Public contenido_medicos As New ContenidoMedicos()
    Public contenido_operadores As New ContenidoOperadores()
    Public contenido_hospitales As New ContenidoHospitales()
    Public contenido_ambulatorios As New ContenidoAmbulatorios()
    Public contenido_especialidades As New ContenidoEspecialidades()
    Public contenido_patologia As New ContenidoPatologias()

    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer


    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim vent_login As New Login()
        vent_login.ShowDialog()


        If autentificado Then
            Panel3.Controls.Clear()
            Panel4.Controls.Clear()


            For Each b As Button In Panel2.Controls.OfType(Of Button)
                If b.Enabled = True Then
                    If Informacion.tema = 0 Then
                        b.BackColor = Color.FromArgb(0, 174, 239)
                        b.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 184, 249)
                    ElseIf Informacion.tema = 1 Then
                        b.BackColor = Color.FromArgb(205, 205, 205)
                        b.FlatAppearance.MouseOverBackColor = Color.FromArgb(219, 219, 219)
                        b.FlatAppearance.MouseDownBackColor = Color.FromArgb(200, 200, 200)
                        b.ForeColor = Color.Gray
                    End If
                End If
            Next


            If Informacion.n_acceso = Informacion.nivel_acceso.admin Then
                'menu Llamadas abierto por defecto
                menu_llamadas.TopLevel = False
                Me.Panel3.Controls.Add(menu_llamadas)
                menu_llamadas.Show()
                cargarContenido("contenido_llamada_urgencia")
                Button1.BackColor = Color.FromArgb(0, 144, 197)
                Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)
                If Informacion.tema = 1 Then
                    Button1.BackColor = Color.FromArgb(103, 103, 103)
                    Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
                    Button1.ForeColor = Color.White
                End If
            ElseIf Informacion.n_acceso = Informacion.nivel_acceso.operador Then
                Button2.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                Button5.Enabled = False

                'menu Llamadas abierto por defecto
                menu_llamadas.TopLevel = False
                Me.Panel3.Controls.Add(menu_llamadas)
                menu_llamadas.Show()
                cargarContenido("contenido_llamada_urgencia")
                Button1.BackColor = Color.FromArgb(0, 144, 197)
                Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)
                If Informacion.tema = 1 Then
                    Button1.BackColor = Color.FromArgb(103, 103, 103)
                    Button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
                    Button1.ForeColor = Color.White
                End If
            ElseIf Informacion.n_acceso = Informacion.nivel_acceso.medico Then
                Button1.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                Button5.Enabled = False

                'menu Medicos abierto por defecto
                menu_medicosProv.TopLevel = False
                Me.Panel3.Controls.Add(menu_medicosProv)
                menu_medicosProv.Show()
                cargarContenido("contenido_asistencias")
                Button2.BackColor = Color.FromArgb(0, 144, 197)
                Button2.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)
                If Informacion.tema = 1 Then
                    Button2.BackColor = Color.FromArgb(103, 103, 103)
                    Button2.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
                    Button2.ForeColor = Color.White
                End If
            End If
        Else
            Application.Exit()
        End If

        For Each b As Button In Panel2.Controls.OfType(Of Button)
            If b.Enabled = False Then
                b.BackColor = Color.Silver
                b.Image = My.Resources.ico_bloqueado
            End If
        Next

        If Informacion.tema = 1 Then
            For Each item In Panel2.Controls
                If TypeOf item Is Panel Then
                    item.BackColor = Color.FromArgb(123, 123, 123)
                End If
            Next

            For Each item In Panel5.Controls
                If TypeOf item Is Panel Then
                    item.BackColor = Color.FromArgb(230, 230, 230)
                End If
            Next

            Panel2.BackColor = Color.FromArgb(205, 205, 205)
            Panel5.BackColor = Color.FromArgb(103, 103, 103)
            PictureBox1.Image = My.Resources.cabecera_Prin_oscuro
        End If



    End Sub

    Private Sub Principal_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub Principal_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub Principal_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless


    Private Sub BotonesMenu_Click(sender As Object, e As EventArgs) Handles Button1.Click, Button2.Click, Button3.Click, Button4.Click, Button5.Click
        For Each b As Button In Panel2.Controls.OfType(Of Button)
            If b.Enabled = True Then
                If Informacion.tema = 0 Then
                    b.BackColor = Color.FromArgb(0, 174, 239)
                    b.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 184, 249)
                ElseIf Informacion.tema = 1 Then
                    b.BackColor = Color.FromArgb(205, 205, 205)
                    b.FlatAppearance.MouseOverBackColor = Color.FromArgb(219, 219, 219)
                    b.ForeColor = Color.Gray
                End If
            End If
        Next

        If Informacion.tema = 0 Then
            Dim boton As Button = CType(sender, Button)
            boton.BackColor = Color.FromArgb(0, 144, 197)
            boton.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 144, 197)

        ElseIf Informacion.tema = 1 Then
            Dim boton As Button = CType(sender, Button)
            boton.BackColor = Color.FromArgb(103, 103, 103)
            boton.FlatAppearance.MouseOverBackColor = Color.FromArgb(103, 103, 103)
            boton.ForeColor = Color.White
        End If


        Dim cual As Integer = CInt(Microsoft.VisualBasic.Strings.Right(CType(sender, Button).Name, 1))
        Select Case cual
            Case 1
                Panel3.Controls.Clear()
                menu_llamadas.TopLevel = False
                Me.Panel3.Controls.Add(menu_llamadas)
                menu_llamadas.Show()
                cargarContenido("contenido_llamada_urgencia")
                menu_llamadas.reset()
            Case 2
                Panel3.Controls.Clear()
                menu_medicosProv.TopLevel = False
                Me.Panel3.Controls.Add(menu_medicosProv)
                menu_medicosProv.Show()
                cargarContenido("contenido_asistencias")
                menu_medicosProv.reset()
            Case 3
                Panel3.Controls.Clear()
                menu_clientes.TopLevel = False
                Me.Panel3.Controls.Add(menu_clientes)
                menu_clientes.Show()
                cargarContenido("contenido_pacientes")
                menu_clientes.reset()
            Case 4
                Panel3.Controls.Clear()
                menu_plantilla.TopLevel = False
                Me.Panel3.Controls.Add(menu_plantilla)
                menu_plantilla.Show()
                cargarContenido("contenido_medicos")
                menu_plantilla.reset()
            Case 5
                Panel3.Controls.Clear()
                menu_tablas.TopLevel = False
                Me.Panel3.Controls.Add(menu_tablas)
                menu_tablas.Show()
                cargarContenido("contenido_hospitales")
                menu_tablas.reset()
        End Select
    End Sub

    'Empezar edicion picture2
    Private Sub PictureBox2_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox2.MouseEnter
        PictureBox2.Image = My.Resources.ajust_over
    End Sub

    Private Sub PictureBox2_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox2.MouseLeave
        PictureBox2.Image = My.Resources.ajust_norm
    End Sub

    Private Sub PictureBox2_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseDown
        PictureBox2.Image = My.Resources.ajust_pres
    End Sub

    Private Sub PictureBox2_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseUp
        PictureBox2.Image = My.Resources.ajust_norm
    End Sub
    'Acabar edicion picture2

    'Empezar edicion picture3
    Private Sub PictureBox3_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox3.MouseEnter
        PictureBox3.Image = My.Resources.des_over
    End Sub

    Private Sub PictureBox3_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox3.MouseLeave
        PictureBox3.Image = My.Resources.des_normal
    End Sub

    Private Sub PictureBox3_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox3.MouseDown
        PictureBox3.Image = My.Resources.desc_pres
    End Sub

    Private Sub PictureBox3_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox3.MouseUp
        PictureBox3.Image = My.Resources.des_normal
    End Sub
    'Acabar edicion picture3

    'Empezar edicion picture4
    Private Sub PictureBox4_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox4.MouseEnter
        PictureBox4.Image = My.Resources.inf_over
    End Sub

    Private Sub PictureBox4_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox4.MouseLeave
        PictureBox4.Image = My.Resources.inf_normal
    End Sub

    Private Sub PictureBox4_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox4.MouseDown
        PictureBox4.Image = My.Resources.inf_pres
    End Sub

    Private Sub PictureBox4_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox4.MouseUp
        PictureBox4.Image = My.Resources.inf_normal
    End Sub
    'Acabar edicion picture4




    Private Sub Panel3_SizeChanged(sender As Object, e As EventArgs) Handles Panel3.SizeChanged
        'Hacemos que se ancle el tamaño de los menus y se ajusten dinamicamente en fullscreen
        menu_llamadas.Size = Me.ClientSize
        menu_medicosProv.Size = Me.ClientSize
        menu_clientes.Size = Me.ClientSize
        menu_plantilla.Size = Me.ClientSize
        menu_tablas.Size = Me.ClientSize
    End Sub

    Private Sub Panel4_SizeChanged(sender As Object, e As EventArgs) Handles Panel4.SizeChanged
        'Hacemos que se ancle el tamaño de los menus y se ajusten dinamicamente en fullscreen
        contenido_llamada_urgencia.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_llamada_asistencia.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_asistencias.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_guardias.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_pacientes.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_personasdecontacto.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_patologiaclientes.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_hospitales.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_ambulatorios.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_especialidades.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_patologia.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_medicos.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
        contenido_operadores.Size = New Size(Me.ClientSize.Width - Panel2.Width, Me.ClientSize.Height - (Panel3.Height + Panel1.Height))
    End Sub

    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Application.Exit()
    End Sub
    'FIN CERRAR

    'INICIO MAXIMIZAR
    Private Sub PictureBox6_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.Image = My.Resources.maximizar_over
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseUp
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseDown
        PictureBox6.Image = My.Resources.maximizar_pulsado
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        If Not (maximizado) Then
            Me.MaximumSize = Screen.FromRectangle(Me.Bounds).WorkingArea.Size 'para que no se maximize encima de la barra de menu de windows
            Me.WindowState = FormWindowState.Maximized
            maximizado = True
        Else
            Me.WindowState = FormWindowState.Normal
            'Me.Size = New Size(1125, 756)
            maximizado = False
        End If
    End Sub
    'FIN MAXIMIZAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    'FIN MINIMIZAR

    Public Sub cargarContenido(ByVal contenido As String)
        Select Case contenido
            Case "contenido_asistencias"
                Panel4.Controls.Clear()
                Me.contenido_asistencias.TopLevel = False
                Me.Panel4.Controls.Add(Me.contenido_asistencias)
                Me.contenido_asistencias.Show()
                Me.contenido_asistencias.ListBox1.Padding = New System.Windows.Forms.Padding(5)

                'recargamos en panel4 del form Principal para que se actualizen los datos visualmente
                Dim contenido_asistencias As New ContenidoAsistencias()
                Panel4.Controls.Clear()
                contenido_asistencias.Size = New Size(ClientSize.Width - Panel2.Width, ClientSize.Height - (Panel3.Height + Panel1.Height))
                contenido_asistencias.TopLevel = False
                Panel4.Controls.Add(contenido_asistencias)
                Me.contenido_asistencias = contenido_asistencias
                contenido_asistencias.Show()
            Case "contenido_guardias"
                Panel4.Controls.Clear()
                contenido_guardias.TopLevel = False
                Me.Panel4.Controls.Add(contenido_guardias)
                contenido_guardias.Show()
            Case "contenido_llamada_urgencia"
                Panel4.Controls.Clear()
                contenido_llamada_urgencia.TopLevel = False
                Me.Panel4.Controls.Add(contenido_llamada_urgencia)
                contenido_llamada_urgencia.Show()
            Case "contenido_llamada_asistencia"
                Me.Panel4.Controls.Clear()
                contenido_llamada_asistencia.TopLevel = False
                Me.Panel4.Controls.Add(contenido_llamada_asistencia)
                contenido_llamada_asistencia.Show()
            Case "contenido_pacientes"
                Panel4.Controls.Clear()
                contenido_pacientes.TopLevel = False
                Me.Panel4.Controls.Add(contenido_pacientes)
                contenido_pacientes.Show()
            Case "contenido_personasdecontacto"
                Panel4.Controls.Clear()
                contenido_personasdecontacto.TopLevel = False
                Me.Panel4.Controls.Add(contenido_personasdecontacto)
                contenido_personasdecontacto.Show()
            Case "contenido_patologiaclientes"
                Panel4.Controls.Clear()
                contenido_patologiaclientes.TopLevel = False
                Me.Panel4.Controls.Add(contenido_patologiaclientes)
                contenido_patologiaclientes.Show()
            Case "contenido_medicos"
                Panel4.Controls.Clear()
                contenido_medicos.TopLevel = False
                Me.Panel4.Controls.Add(contenido_medicos)
                contenido_medicos.Show()
            Case "contenido_operadores"
                Panel4.Controls.Clear()
                contenido_operadores.TopLevel = False
                Me.Panel4.Controls.Add(contenido_operadores)
                contenido_operadores.Show()
            Case "contenido_hospitales"
                Panel4.Controls.Clear()
                contenido_hospitales.TopLevel = False
                Me.Panel4.Controls.Add(contenido_hospitales)
                contenido_hospitales.Show()
            Case "contenido_ambulatorios"
                Panel4.Controls.Clear()
                contenido_ambulatorios.TopLevel = False
                Me.Panel4.Controls.Add(contenido_ambulatorios)
                contenido_ambulatorios.Show()
            Case "contenido_especialidades"
                Panel4.Controls.Clear()
                contenido_especialidades.TopLevel = False
                Me.Panel4.Controls.Add(contenido_especialidades)
                contenido_especialidades.Show()
            Case "contenido_patologia"
                Panel4.Controls.Clear()
                contenido_patologia.TopLevel = False
                Me.Panel4.Controls.Add(contenido_patologia)
                contenido_patologia.Show()
            Case Else
                Debug.Print("La cadena de texto no corresponde a ningún contenido")
        End Select
    End Sub

    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles PictureBox8.Click
        Dim menu_telf As New Telefono()
        menu_telf.Show()
    End Sub

    'Empezar edicion picture8
    Private Sub PictureBox8_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox8.MouseEnter
        PictureBox8.Image = My.Resources.llamada_over
    End Sub

    Private Sub PictureBox8_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox8.MouseLeave
        PictureBox8.Image = My.Resources.llamada_normal
    End Sub

    Private Sub PictureBox8_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox8.MouseDown
        PictureBox8.Image = My.Resources.llamada_press
    End Sub

    Private Sub PictureBox8_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox8.MouseUp
        PictureBox8.Image = My.Resources.llamada_normal
    End Sub
    'Acabar edicion picture8

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea cerrar la sesión?", MsgBoxStyle.YesNo, "Cerrar sesión")
        If comprobacion = 6 Then
            Application.Restart()
        End If
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Dim acercaDe As New AcercaDe
        acercaDe.ShowDialog()
    End Sub

    Private Sub Principal_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea salir del programa? Se perderán los cambios no guardados.", MsgBoxStyle.YesNo, "Salir del programa")
            If comprobacion = 6 Then
                Application.Exit()
            End If
        End If
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Dim menu_ajustes As New Ajustes
        menu_ajustes.ShowDialog()
    End Sub
End Class