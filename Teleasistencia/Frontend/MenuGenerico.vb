﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################


Public Class MenuGenerico
    Private Sub Label_MouseEnter(sender As Object, e As EventArgs) Handles Label2.MouseEnter, Label3.MouseEnter, Label4.MouseEnter, Label5.MouseEnter
        Dim l As Label = CType(sender, Label)
        l.ForeColor = Color.FromArgb(181, 181, 181)
    End Sub

    Private Sub Label_MouseLeave(sender As Object, e As EventArgs) Handles Label2.MouseLeave, Label3.MouseLeave, Label4.MouseLeave, Label5.MouseLeave
        Dim l As Label = CType(sender, Label)
        l.ForeColor = Color.DimGray
    End Sub

    Private Sub Label_MouseDown(sender As Object, e As EventArgs) Handles Label2.MouseDown, Label3.MouseDown, Label4.MouseDown, Label5.MouseDown
        Dim l As Label = CType(sender, Label)
        l.ForeColor = Color.Black
        PanelSelect.Size = New Size(l.Size.Width, PanelSelect.Size.Height)
        PanelSelect.Location = New Point(l.Location.X, PanelSelect.Location.Y)
    End Sub

    Private Sub Label_MouseUp(sender As Object, e As MouseEventArgs) Handles Label2.MouseUp, Label3.MouseUp, Label4.MouseUp, Label5.MouseUp
        Dim l As Label = CType(sender, Label)
        l.ForeColor = Color.DimGray
    End Sub
End Class