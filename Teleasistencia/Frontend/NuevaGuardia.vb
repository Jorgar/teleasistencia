﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################




Imports System.ComponentModel

Public Class NuevaGuardia

    'Variables de clase
    Private maximizado As Boolean = False
    Private bbdd As New Datos()
    Private val_hi As Boolean
    Private val_hf As Boolean
    Private val_dtpckr As Boolean
    Private dia, mes, anio As String
    Private fecha_formateada As String

    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer

    Private Sub NuevaGuardia_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub NuevaGuardia_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub NuevaGuardia_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless


    Private Sub NuevaGuardia_Load(sender As Object, e As EventArgs) Handles Me.Load
        ComboBox1.Text = Informacion.login
        val_hi = False
        val_hf = False
        val_dtpckr = True
    End Sub


    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Me.Close()
    End Sub
    'FIN CERRAR

    'INICIO MAXIMIZAR
    Private Sub PictureBox6_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.Image = My.Resources.maximizar_over
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseUp
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseDown
        PictureBox6.Image = My.Resources.maximizar_pulsado
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        If Not (maximizado) Then
            Me.MaximumSize = Screen.FromRectangle(Me.Bounds).WorkingArea.Size 'para que no se maximize encima de la barra de menu de windows
            Me.WindowState = FormWindowState.Maximized
            maximizado = True
        Else
            Me.WindowState = FormWindowState.Normal
            'Me.Size = New Size(1125, 756)
            maximizado = False
        End If
    End Sub
    'FIN MAXIMIZAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea cancelar la nueva guardia?", MsgBoxStyle.YesNo, "Cancelar nueva guardia")
        If comprobacion = 6 Then
            Me.Close()
        End If
    End Sub
    'FIN MINIMIZAR

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (val_hi And val_hf And val_dtpckr) Then


            Dim hora_inicio As String = Me.MaskedTextBox3.Text
            Dim hora_final As String = Me.MaskedTextBox1.Text

            dia = DateTimePicker1.Value.Day.ToString
            mes = DateTimePicker1.Value.Month.ToString
            anio = DateTimePicker1.Value.Year.ToString

            formatear_fecha(dia, mes, anio)

            bbdd.nuevaGuardia(fecha_formateada, ComboBox1.Text, hora_inicio, hora_final)

            Dim contenido_guardias As New ContenidoGuardias()
            Principal.Panel4.Controls.Clear()
            contenido_guardias.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
            contenido_guardias.TopLevel = False
            Principal.Panel4.Controls.Add(contenido_guardias)
            Principal.contenido_guardias = contenido_guardias
            contenido_guardias.Show()

            Me.Close()
        Else
            Label5.Visible = True
        End If
    End Sub

    Private Sub MaskedTextBox1_Validating(sender As Object, e As CancelEventArgs) Handles MaskedTextBox1.Validating
        If MaskedTextBox1.MaskCompleted Then
            val_hi = True
            Label5.Visible = False

            If CInt(MaskedTextBox1.Text.Substring(0, 2)) > 23 And CInt(MaskedTextBox1.Text.Substring(3, 2)) > 59 Then
                MaskedTextBox1.Text = "0059"
            ElseIf CInt((MaskedTextBox1.Text.Substring(0, 2))) > 23 And CInt(MaskedTextBox1.Text.Substring(3, 2)) < 59 Then
                Dim hora_val As String = "00" + MaskedTextBox1.Text.Substring(2, 3)
                MaskedTextBox1.Text = hora_val
            ElseIf CInt((MaskedTextBox1.Text.Substring(0, 2))) < 23 And CInt(MaskedTextBox1.Text.Substring(3, 2)) > 59 Then
                Dim minutos_val As String = MaskedTextBox1.Text.Substring(0, 2) + "59"
                MaskedTextBox1.Text = minutos_val
            End If
        Else
            Label5.Visible = True
        End If
    End Sub

    Private Sub MaskedTextBox3_Validating(sender As Object, e As CancelEventArgs) Handles MaskedTextBox3.Validating
        If MaskedTextBox3.MaskCompleted Then
            val_hf = True
            Label5.Visible = False

            If CInt(MaskedTextBox3.Text.Substring(0, 2)) > 23 And CInt(MaskedTextBox3.Text.Substring(3, 2)) > 59 Then
                MaskedTextBox3.Text = "0059"
            ElseIf CInt((MaskedTextBox3.Text.Substring(0, 2))) > 23 And CInt(MaskedTextBox3.Text.Substring(3, 2)) < 59 Then
                Dim hora_val As String = "00" + MaskedTextBox3.Text.Substring(2, 3)
                MaskedTextBox3.Text = hora_val
            ElseIf CInt((MaskedTextBox3.Text.Substring(0, 2))) < 23 And CInt(MaskedTextBox3.Text.Substring(3, 2)) > 59 Then
                Dim minutos_val As String = MaskedTextBox3.Text.Substring(0, 2) + "59"
                MaskedTextBox3.Text = minutos_val
            End If
        Else
            Label5.Visible = True
        End If
    End Sub

    Private Sub formatear_fecha(ByRef dia As String, ByRef mes As String, ByRef anio As String)
        If (dia.Length < 2) Then
            dia = "0" + dia
        End If
        If (mes.Length < 2) Then
            mes = "0" + mes
        End If
        fecha_formateada = anio + mes + dia
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged
        dia = DateTimePicker1.Value.Day.ToString
        mes = DateTimePicker1.Value.Month.ToString
        anio = DateTimePicker1.Value.Year.ToString

        formatear_fecha(dia, mes, anio)

        If (bbdd.comprobarGuardia(ComboBox1.Text, fecha_formateada)) Then
            val_dtpckr = False
            Label7.Visible = True
        Else
            val_dtpckr = True
            Label7.Visible = False
        End If
    End Sub


    Private Sub form_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        Select Case e.KeyChar = ControlChars.Cr
            Case True
                System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
        End Select
    End Sub

End Class