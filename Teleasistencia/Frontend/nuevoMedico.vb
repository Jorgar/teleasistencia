﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################






Public Class nuevoMedico

    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Dim bbdd As New Datos
    Private fecha_formateada As String
    Private dia, mes, anio As String

    Private Sub NuevaGuardia_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub NuevaGuardia_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub NuevaGuardia_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless

    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Me.Close()
    End Sub
    'FIN CERRAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    'FIN MINIMIZAR

    Function validarCampos() As Boolean
        Dim validado = True
        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            If campo.Text = Nothing Then
                                                campo.BackColor = Color.SeaShell
                                                subobjeto.BackColor = Color.SeaShell
                                                validado = False
                                            Else
                                                campo.BackColor = Color.White
                                                subobjeto.BackColor = Color.White
                                            End If
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            If campo.SelectedItem = Nothing Then
                                                campo.BackColor = Color.SeaShell
                                                subobjeto.BackColor = Color.SeaShell
                                                validado = False
                                            Else
                                                campo.BackColor = Color.White
                                                subobjeto.BackColor = Color.White
                                            End If
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
        Return validado
    End Function

    Private Sub Filtrado_Numeros_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress, TextBox3.KeyPress
        Select Case e.KeyChar.IsNumber(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

    Private Sub Filtrado_Letras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress, TextBox4.KeyPress
        Select Case e.KeyChar.IsLetter(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Chr(Keys.Space)
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

    Private Sub borrar_campos()   'borra el contenido de los controles
        For Each objeto In TableLayoutPanel2.Controls
            Select Case TypeOf objeto Is Panel
                Case True
                    For Each subobjeto In objeto.Controls
                        Select Case TypeOf subobjeto Is Panel
                            Case True
                                For Each campo In subobjeto.Controls
                                    Select Case TypeOf campo Is TextBox
                                        Case True
                                            campo.Text = ""
                                    End Select

                                    Select Case TypeOf campo Is ComboBox
                                        Case True
                                            campo.SelectedItem = Nothing
                                    End Select
                                Next
                        End Select
                    Next
            End Select
        Next
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Not validarCampos() Then
            MsgBox("Rellene los campos vacíos", MsgBoxStyle.Critical, "Error en los campos")
            Exit Sub
        End If

        If Not comprobarDNI(TextBox9.Text) Then
            nuevoMedico()

            'recargamos en panel4 del form Principal para que se actualizen los datos visualmente
            Dim contenido_medicos As New ContenidoMedicos()
            Principal.Panel4.Controls.Clear()
            contenido_medicos.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
            contenido_medicos.TopLevel = False
            Principal.Panel4.Controls.Add(contenido_medicos)
            Principal.contenido_medicos = contenido_medicos
            contenido_medicos.Show()

            Me.Close()
        Else
            MsgBox("El usuario con dni '" + TextBox9.Text + "' ya existe en la base de datos.", MsgBoxStyle.Critical, "Error en los campos")
        End If
    End Sub

    Function comprobarDNI(ByVal dni As String) As Boolean
        Dim existe = False
        If (bbdd.devolverTabla("SELECT * FROM medicos WHERE dni='" + dni + "'").Rows.Count > 0 And Not dni.Equals(TextBox9.Text)) Then
            existe = True
        End If
        Return existe
    End Function

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea borrar todos los campos?", MsgBoxStyle.YesNo, "Borrar campos")
        If comprobacion = 6 Then
            borrar_campos()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea cancelar el nuevo médico?", MsgBoxStyle.YesNo, "Cancelar creación de nuevo médico")
        If comprobacion = 6 Then
            Me.Close()
        End If
    End Sub

    Private Sub nuevoPaciente_Load(sender As Object, e As EventArgs) Handles Me.Load
        bbdd.cargarComboBox("SELECT nombre FROM especialidades", ComboBox2)
        bbdd.cargarComboBox("SELECT nombre FROM hospitales", ComboBox1)
    End Sub

    Private Sub nuevoMedico()
        Try
            Dim dni As String = TextBox9.Text
            Dim apellidos As String = TextBox4.Text
            Dim nombre As String = TextBox2.Text
            Dim domicilio As String = TextBox7.Text
            Dim poblacion As String = TextBox6.Text
            Dim cod_postal As String = TextBox3.Text
            Dim telefono As Int64 = CType(TextBox1.Text, Int64)

            'Especialidad
            Dim codEsp As Integer = bbdd.devolverTabla("SELECT codigo FROM especialidades WHERE nombre='" + ComboBox2.Text + "'").Rows(0)(0)
            'Hospital
            Dim codHosp As Integer = bbdd.devolverTabla("SELECT codigo FROM hospitales WHERE nombre='" + ComboBox1.Text + "'").Rows(0)(0)

            Dim email As String = TextBox10.Text
            Try
                bbdd.nuevoMedico(dni, apellidos, nombre, domicilio, poblacion, cod_postal,
                           telefono, codEsp, codHosp, email)
                MsgBox("Datos guardado satisfactoriamente.", MsgBoxStyle.DefaultButton1, "Actualización")
            Catch ex As Exception
                MsgBox("Error al actulizar datos.", MsgBoxStyle.Critical, "Error en actualización")
            End Try
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error en la base de datos MySQL")
            Debug.Print(ex.ToString & vbNewLine & ex.Message)
        End Try
    End Sub
End Class