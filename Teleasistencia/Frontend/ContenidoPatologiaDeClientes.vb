﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################



Public Class ContenidoPatologiaDeClientes

    'variables
    Dim bbdd As New Datos()
    Private dni_cliente As String
    Private patologia_principal As String
    Private nombre, apellidos As String

    Private Sub ContenidoPatologiaDeClientes_Load(sender As Object, e As EventArgs) Handles Me.Load
        bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM clientes", ListBox1, True)

        bbdd.cargarComboBox("SELECT descripcion FROM patologias", ComboBox2)

        If Not ComboBox2.SelectedItem Is Nothing Then
            bbdd.cargarListBox("SELECT descripcion FROM patologias WHERE descripcion!='" + ComboBox2.Text.Trim + "'", ListBox2, False)
        Else
            bbdd.cargarListBox("SELECT descripcion FROM patologias", ListBox2, False)
        End If

        ListBox1.SelectedIndex = 0
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        Dim array() As String = ListBox1.SelectedItem.ToString.Split(", ")
        apellidos = array(0).Trim
        nombre = array(1).Trim

        cargar_datos_paciente(nombre, apellidos)
        seleccionarPrincipal()
        seleccionarSecundarias()
    End Sub

    Sub cargar_datos_paciente(ByVal nombre As String, ByVal apellidos As String)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT * FROM clientes WHERE nombre ='" + nombre + "' AND
        apellidos ='" + apellidos + "'")

        For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos
            Select Case i
                Case 0  'dni
                    TextBox10.Text = tabla_datos.Rows(0)(i).ToString
                    dni_cliente = tabla_datos.Rows(0)(i).ToString
                Case 1  'apellidos
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString
                Case 2  'nombre
                    TextBox6.Text = TextBox6.Text + ", " + tabla_datos.Rows(0)(i).ToString
                Case 3  'domicilio
                    TextBox14.Text = tabla_datos.Rows(0)(i).ToString
                Case 4  'poblacion
                    TextBox14.Text += ", " + tabla_datos.Rows(0)(i).ToString
                Case 6 'telefono
                    TextBox11.Text = "Tel: " + tabla_datos.Rows(0)(i).ToString
                Case 9  'fecha nac
                    Dim dia, mes, anio As String
                    anio = tabla_datos.Rows(0)(i).ToString.Substring(0, 4)
                    mes = tabla_datos.Rows(0)(i).ToString.Substring(4, 2)
                    dia = tabla_datos.Rows(0)(i).ToString.Substring(6, 2)
                    MaskedTextBox5.Text = dia + mes + anio
                Case 10  'sexo
                    If (tabla_datos.Rows(0)(i).ToString.Equals("H")) Then
                        TextBox12.Text = "HOMBRE"
                    Else
                        TextBox12.Text = "MUJER"
                    End If
            End Select
        Next
        Try
            Dim patologia_prin As String = bbdd.devolverTabla("SELECT patologias.descripcion FROM patologias INNER JOIN patologias_clientes ON patologias.codigo = patologias_clientes.patologia AND patologias_clientes.descripcion = 'PRINCIPAL' AND patologias_clientes.dni_cliente = '" + dni_cliente + "'").Rows(0)(0).ToString
            Label1.Text = "Patología principal: " + Char.ToUpper(patologia_prin(0)) & patologia_prin.Substring(1)
            patologia_principal = patologia_prin
            Dim tabla_p_secundarias As DataTable = bbdd.devolverTabla("SELECT patologias.descripcion FROM patologias INNER JOIN patologias_clientes ON patologias.codigo = patologias_clientes.patologia AND (patologias_clientes.descripcion IS NULL OR patologias_clientes.descripcion!='PRINCIPAL')  AND patologias_clientes.dni_cliente='" + dni_cliente + "'")
            If tabla_p_secundarias.Rows.Count > 0 Then
                Label1.Text += vbCrLf + "Patologías secundarias: "
                For i As Integer = 0 To tabla_p_secundarias.Rows.Count - 1
                    Label1.Text += tabla_p_secundarias.Rows(i)(0).ToString + ", "
                Next
                Label1.Text = Label1.Text.Substring(0, Label1.Text.Length - 2)
            End If
        Catch ex As Exception
            Label1.Text = "Patología principal: ns/nc"
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Sub seleccionarPrincipal()
        ComboBox2.SelectedItem = patologia_principal
    End Sub

    Sub seleccionarSecundarias()
        ListBox2.SelectedItems.Clear()
        Dim tabla_p_secundarias As DataTable = bbdd.devolverTabla("SELECT patologias.descripcion FROM patologias INNER JOIN patologias_clientes ON
                patologias.codigo = patologias_clientes.patologia AND (patologias_clientes.descripcion IS NULL OR patologias_clientes.descripcion!='PRINCIPAL')  AND
                patologias_clientes.dni_cliente='" + TextBox10.Text.Trim + "'")
        If tabla_p_secundarias.Rows.Count > 0 Then
            For i As Integer = 0 To tabla_p_secundarias.Rows.Count - 1
                Dim index As Integer = ListBox2.FindString(tabla_p_secundarias.Rows(i)(0).ToString)
                ListBox2.SelectedIndex = index
            Next
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted eliminar todas las patologías seleccionadas?", MsgBoxStyle.YesNo, "Cancelar patologías")
        If comprobacion = 6 Then
            ComboBox2.SelectedIndex = 0
            ListBox2.SelectedItems.Clear()
        End If
    End Sub

    Private Sub guardarDatos(ByVal dni_paciente As String, ByVal combo As ComboBox)
        Dim codigoPrin As Integer = bbdd.devolverTabla("SELECT codigo FROM patologias WHERE descripcion='" + combo.SelectedItem.ToString.Trim + "'").Rows(0)(0).ToString
        bbdd.borrarTodasPatologiasSecundarias(dni_paciente)
        bbdd.borrarPatologiaPrincipal(dni_paciente)
        bbdd.insertarPatologiaPrincipal(dni_paciente, codigoPrin)
        For Each selectedItem In ListBox2.SelectedItems
            Dim codigoSec As Integer = bbdd.devolverTabla("SELECT codigo FROM patologias WHERE descripcion='" + selectedItem.ToString.Trim + "'").Rows(0)(0).ToString
            bbdd.insertarPatologiaSecundaria(dni_paciente, codigoSec)
        Next
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If Not TextBox5.Text.Equals("") Then
            ListBox1.Items.Clear()
            bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM clientes WHERE dni='" + TextBox5.Text + "' OR nombre='" + TextBox5.Text + "' OR  apellidos='" + TextBox5.Text + "'", ListBox1, True)
        Else
            ListBox1.Items.Clear()
            bbdd.cargarListBox("SELECT CONCAT(apellidos,', ',nombre) FROM clientes", ListBox1, True)
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            guardarDatos(TextBox10.Text.Trim, ComboBox2)
            MsgBox("Datos guardados satisfactoriamente", MsgBoxStyle.DefaultButton1, "Datos guardados")

            'recargamos en panel4 del form principal para que se actualizen los datos visualmente
            Dim contenido_patologiaclientes As New ContenidoPatologiaDeClientes()
            Principal.Panel4.Controls.Clear()
            contenido_patologiaclientes.Size = New Size(Principal.ClientSize.Width - Principal.Panel2.Width, Principal.ClientSize.Height - (Principal.Panel3.Height + Principal.Panel1.Height))
            contenido_patologiaclientes.TopLevel = False
            Principal.Panel4.Controls.Add(contenido_patologiaclientes)
            Principal.contenido_patologiaclientes = contenido_patologiaclientes
            contenido_patologiaclientes.Show()
        Catch ex As Exception
            MsgBox("Error al guardar los datos", MsgBoxStyle.Critical, "Error Guardar")
        End Try

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        ListBox2.Items.Clear()
        bbdd.cargarListBox("SELECT descripcion FROM patologias WHERE descripcion!='" + ComboBox2.Text.Trim + "'", ListBox2, False)
    End Sub

    Private Sub TextBox5_Click(sender As Object, e As EventArgs) Handles TextBox5.Click
        TextBox5.Text = ""
        TextBox5.ForeColor = Color.DimGray
    End Sub

End Class