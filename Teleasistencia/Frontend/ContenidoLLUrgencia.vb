﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################



Public Class ContenidoLLUrgencia

    'variables
    Public bbdd As New Datos()

    'carga del formulario
    Private Sub ContenidoLLUrgencia_Load(sender As Object, e As EventArgs) Handles Me.Load
        rellenar_estado()               'Rellenamos el combo de estado de la urgencia
        ComboBox1.SelectedIndex = 0     'Preseleciones /Sin selección
        bbdd.cargarListBox("SELECT numero FROM urgencias WHERE (SUBSTRING(fecha,1,4)=YEAR(CURRENT_DATE()) AND SUBSTRING(fecha, 5, 2)=MONTH(CURRENT_DATE())) 
                              OR (estado=1 OR estado=0) ORDER BY numero DESC", ListBox1, True)        'cargamos el listbox con las urgencias
        If ListBox1.Items.Count > 0 Then
            ListBox1.SelectedIndex = 0
        End If
    End Sub

    Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ll_urg As New Llamada112()
        ll_urg.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim ll_contacto As New LlamadaContacto(TextBox5.Text)
        ll_contacto.Show()
    End Sub

    'Método que rellena el combobox de 
    'estado de asistencia/urgencia a partir
    'de su enumaracion.
    Sub rellenar_estado()
        For Each item In System.Enum.GetValues(GetType(Informacion.estado_alarma))
            ComboBox1.Items.Add(Informacion.getDescripcion(CType(item, [Enum])))
        Next
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        cargar_datos_paciente(CInt(ListBox1.SelectedItem.ToString))
        cargar_datos_urgencia(CInt(ListBox1.SelectedItem.ToString))
    End Sub

    Sub cargar_datos_paciente(ByVal numero_urgencia As Integer)
        Try
            Dim tabla_datos As Data.DataTable

            tabla_datos = bbdd.devolverTabla("SELECT dni, telefono, nombre, apellidos,domicilio,poblacion FROM clientes 
                                 INNER JOIN urgencias On clientes.dni=urgencias.dni_cliente And urgencias.numero =" + numero_urgencia.ToString)

            For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos
                Select Case i
                    Case 0
                        TextBox5.Text = tabla_datos.Rows(0)(i).ToString
                    Case 1
                        TextBox6.Text = tabla_datos.Rows(0)(i).ToString
                    Case 2
                        TextBox2.Text = tabla_datos.Rows(0)(i).ToString
                    Case 3
                        TextBox3.Text = tabla_datos.Rows(0)(i).ToString
                    Case 5
                        TextBox4.Text = tabla_datos.Rows(0)(i - 1).ToString + ", " + tabla_datos.Rows(0)(i).ToString
                End Select
            Next
        Catch ex As Exception
            Debug.Print(ex.ToString)
        End Try

    End Sub


    Sub cargar_datos_urgencia(ByVal numero_urgencia As Integer)
        Try
            Dim tabla_datos As Data.DataTable
            tabla_datos = bbdd.devolverTabla("SELECT numero, estado, fecha,hora FROM 
                                            urgencias WHERE numero=" + numero_urgencia.ToString)

            For i As Integer = 0 To tabla_datos.Columns.Count - 1
                Select Case i
                    Case 0
                        TextBox9.Text = tabla_datos.Rows(0)(i).ToString
                    Case 1
                        ComboBox1.SelectedIndex = CInt(tabla_datos.Rows(0)(i))
                    Case 2
                        Dim dia, mes, anio As String
                        anio = tabla_datos.Rows(0)(i).ToString.Substring(0, 4)
                        mes = tabla_datos.Rows(0)(i).ToString.Substring(4, 2)
                        dia = tabla_datos.Rows(0)(i).ToString.Substring(6, 2)
                        MaskedTextBox1.Text = dia + mes + anio
                    Case 3
                        MaskedTextBox2.Text = tabla_datos.Rows(0)(i).ToString
                End Select
            Next
        Catch ex As Exception
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim cod_hospital As Int64
        Try
            bbdd.actualizarUrgencia(CInt(TextBox9.Text), ComboBox1.SelectedIndex)
            MsgBox("Datos guardado satisfactoriamente.", MsgBoxStyle.DefaultButton1, "Actualización")
        Catch ex As Exception
            MsgBox("Error al guardar los datos.", MsgBoxStyle.Critical, "Actualización")
        End Try
    End Sub





End Class