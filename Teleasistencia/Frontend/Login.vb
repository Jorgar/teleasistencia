﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################



Imports System.ComponentModel

Public Class Login

    'Variables
    Private tbAct1 As Boolean = True
    Private tbAct2 As Boolean = True
    Dim bbdd As New Datos()

    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer


    Private Sub Login_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub Login_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub Login_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.ForeColor = Color.Silver
        TextBox2.ForeColor = Color.Silver
        PictureBox1.Select()
    End Sub

    Private Sub PictureBox2_MouseHover(sender As Object, e As EventArgs) Handles PictureBox2.MouseHover
        PictureBox2.Image = My.Resources.X_hover
    End Sub

    Private Sub PictureBox2_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox2.MouseLeave
        PictureBox2.Image = My.Resources.X_normal
    End Sub

    Private Sub PictureBox2_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseDown
        PictureBox2.Image = My.Resources.X_press
    End Sub

    Private Sub PictureBox2_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseUp
        PictureBox2.Image = My.Resources.X_normal
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.GotFocus
        If (tbAct1) Then
            TextBox1.Text = ""
            TextBox1.ForeColor = Color.DimGray
            tbAct1 = False
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.GotFocus
        If (tbAct2) Then
            TextBox2.Text = ""
            TextBox2.ForeColor = Color.DimGray
            tbAct2 = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        bbdd.comprobarConexion()    'comprobamos si hay conexion con el servidor

        If comprobarMedico(TextBox1.Text.Trim) Then 'Es medico
            Informacion.login = TextBox1.Text.Trim
            Informacion.n_acceso = Informacion.nivel_acceso.medico
            Principal.autentificado = True
            leerTema()
        ElseIf comprobarOperador(TextBox1.Text.Trim) Then
            If comprobarContrasenia(TextBox1.Text.Trim) Then
                Principal.autentificado = True
                Informacion.n_acceso = bbdd.devolverTabla("SELECT nivel_acceso FROM operadores WHERE login='" + TextBox1.Text.Trim + "'").Rows(0)(0)
                Informacion.login = TextBox1.Text.Trim
                Principal.autentificado = True
                leerTema()
            Else
                Principal.autentificado = False
            End If
        Else
            Principal.autentificado = False
        End If
        Me.Close()
    End Sub

    Sub leerTema()
        Try
            FileOpen(1, "ajustes.dat", OpenMode.Input)
            Input(1, Informacion.tema)
            FileClose(1)
        Catch ex As Exception
            Debug.Print(ex.ToString)
        End Try
    End Sub


    Function comprobarMedico(ByRef dni As String)
        Dim esMedico As Boolean = False
        Try
            Dim nombreMedico As String = bbdd.devolverTabla("SELECT nombre FROM medicos WHERE dni='" + dni + "'").Rows(0)(0).ToString
            esMedico = True
            Return esMedico
        Catch ex As Exception
            esMedico = False
            Return esMedico
        End Try
    End Function

    Function comprobarOperador(ByRef login As String)
        Dim esOperador As Boolean = False
        Try
            Dim nombreOperador As String = bbdd.devolverTabla("SELECT nombre FROM operadores WHERE login='" + login + "'").Rows(0)(0).ToString
            esOperador = True
            Return esOperador
        Catch ex As Exception
            esOperador = False
            Return esOperador
        End Try
    End Function

    Function comprobarContrasenia(ByRef login As String)
        Dim esCorrecto As Boolean = False
        Dim contrasniaData As String = bbdd.devolverTabla("SELECT password FROM operadores WHERE login='" + login + "'").Rows(0)(0).ToString
        If contrasniaData = TextBox2.Text Then
            esCorrecto = True
        Else
            esCorrecto = False
        End If
        Return esCorrecto
    End Function

End Class