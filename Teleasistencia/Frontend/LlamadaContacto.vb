﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################



Public Class LlamadaContacto

    'Variables
    Private llamando As Boolean
    Private dni_cliente As String
    Private bbdd As New Datos()
    Private dni_contacto As String

    'Mover ventana form borderless
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Private maximizado As Boolean = False

    Private Sub NuevaGuardia_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub NuevaGuardia_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub NuevaGuardia_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseMove
        If IsFormBeingDragged Then
            Dim temp As Point = New Point()
            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub
    '/Mover ventana form borderless

    Sub New(ByRef dni_cliente As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.dni_cliente = dni_cliente
    End Sub

    Private Sub LlamadaContacto_Load(sender As Object, e As EventArgs) Handles Me.Load
        If ComboBox1.Items.Count > 0 Then
            ComboBox1.SelectedIndex = 0
            ComboBox6.SelectedIndex = 0
        End If
        llamando = False
        bbdd.cargarComboBox("SELECT dni FROM personas_contacto WHERE dni_cliente='" + dni_cliente.ToString + "'", ComboBox1)
        Dim p_contacto As String = bbdd.devolverTabla("Select dni FROM personas_contacto WHERE dni_cliente='" + dni_cliente.ToString + "'").Rows(0)(0).ToString
        cargar_datos_persona_contacto(dni_cliente, p_contacto)
        ComboBox1.SelectedIndex = 0
    End Sub

    'CERRAR, MAXIMIZAR Y MINIMIZAR
    'INICIO CERRAR
    Private Sub PictureBox5_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.Image = My.Resources.cerrar_over
    End Sub

    Private Sub PictureBox5_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox5.MouseLeave
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseUp
        PictureBox5.Image = My.Resources.cerrar_normal
    End Sub

    Private Sub PictureBox5_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox5.MouseDown
        PictureBox5.Image = My.Resources.cerrar_pulsado
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Me.Close()
    End Sub
    'FIN CERRAR

    'INICIO MAXIMIZAR
    Private Sub PictureBox6_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.Image = My.Resources.maximizar_over
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseUp
        PictureBox6.Image = My.Resources.maximizar_normal
    End Sub

    Private Sub PictureBox6_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox6.MouseDown
        PictureBox6.Image = My.Resources.maximizar_pulsado
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        If Not (maximizado) Then
            Me.MaximumSize = Screen.FromRectangle(Me.Bounds).WorkingArea.Size 'para que no se maximize encima de la barra de menu de windows
            Me.WindowState = FormWindowState.Maximized
            maximizado = True
        Else
            Me.WindowState = FormWindowState.Normal
            'Me.Size = New Size(1125, 756)
            maximizado = False
        End If
    End Sub
    'FIN MAXIMIZAR

    'INICIO MINIMIZAR
    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.minimizar_over
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseUp
        PictureBox7.Image = My.Resources.minimizar_normal
    End Sub

    Private Sub PictureBox7_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox7.MouseDown
        PictureBox7.Image = My.Resources.minimizar_pulsado
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    'FIN MINIMIZAR

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        llamando = False
        Label11.Text = "Llamando a"
        Label11.Visible = False
        PictureBox1.Visible = False
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

        cargar_datos_persona_contacto(dni_cliente, ComboBox1.SelectedItem.ToString)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not llamando Then
            Dim tel_contacto As String = ComboBox6.Text
            Label11.Text += " " + tel_contacto + "..."
            Label11.Visible = True
            PictureBox1.Visible = True
            llamando = True
        End If
    End Sub

    Sub cargar_datos_persona_contacto(ByVal dni_cliente As String, ByVal dni_con As String)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT personas_contacto.dni, personas_contacto.nombre, personas_contacto.apellidos, personas_contacto.
                                            domicilio,personas_contacto.poblacion,personas_contacto.cod_postal,telefono1,
                                            telefono2,parentesco FROM personas_contacto INNER JOIN clientes ON  personas_contacto.dni_cliente =" +
                                            "'" + dni_cliente + "' AND personas_contacto.dni ='" + dni_con + "'")

        For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos

            Select Case i
                Case 0
                    TextBox9.Text = tabla_datos.Rows(0)(i).ToString
                Case 1
                    TextBox3.Text = tabla_datos.Rows(0)(i).ToString
                Case 2
                    TextBox4.Text = tabla_datos.Rows(0)(i).ToString
                Case 4
                    TextBox5.Text = tabla_datos.Rows(0)(i - 1).ToString + ", " + tabla_datos.Rows(0)(i).ToString
                Case 5
                    TextBox2.Text = tabla_datos.Rows(0)(i).ToString
                Case 7
                    ComboBox6.Items.Clear()
                    ComboBox6.Items.Add(tabla_datos.Rows(0)(i - 1).ToString)
                    ComboBox6.Items.Add(tabla_datos.Rows(0)(i).ToString)
                    ComboBox6.SelectedIndex = 0
                Case 8
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString
            End Select
        Next

        TextBox1.Text = dni_cliente
    End Sub

End Class