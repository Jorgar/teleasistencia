﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################




Public Class ContenidoLLAsistencia

    'Variables
    Dim bbdd As New Datos()
    Private fechaHoy As DateTime = DateTime.Today
    Private diaHoy As String = fechaHoy.Day.ToString
    Private mesHoy As String = fechaHoy.Month.ToString
    Private anioHoy As String = fechaHoy.Year.ToString


    'Carga del formulario
    Private Sub ContenidoLLAsistencia_Load(sender As Object, e As EventArgs) Handles Me.Load
        rellenar_estado()             'Rellenar estado de asistencia
        rellenar_tipo()               'Rellenar tipo de asistencia
        bbdd.cargarListBox("SELECT numero FROM asistencias WHERE (SUBSTRING(fecha,1,4)=YEAR(CURRENT_DATE()) AND SUBSTRING(fecha, 5, 2)=MONTH(CURRENT_DATE())) 
                              OR (tipo=1 OR tipo=0) ORDER BY numero DESC", ListBox1, True)        'cargamos el listbox con las asistencias
        If ListBox1.Items.Count > 0 Then
            ListBox1.SelectedIndex = 0
        End If
    End Sub

    'Método que rellena el combobox de 
    'estado de asistencia/urgencia a partir
    'de su enumaracion.
    Sub rellenar_estado()
        For Each item In System.Enum.GetValues(GetType(Informacion.estado_alarma))
            ComboBox1.Items.Add(Informacion.getDescripcion(CType(item, [Enum])))
        Next
    End Sub

    'Método que rellena el combobox de 
    'tipo de asistencia/urgencia a partir
    'de su enumaracion.
    Sub rellenar_tipo()
        For Each item In System.Enum.GetValues(GetType(Informacion.tipo_urgencia))
            If Not item.ToString.Equals("urgencia") Then
                ComboBox3.Items.Add(Informacion.getDescripcion(CType(item, [Enum])))
            End If

        Next
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        cargar_datos_paciente(CInt(ListBox1.SelectedItem.ToString))
        cargar_datos_asistencia(CInt(ListBox1.SelectedItem.ToString))
    End Sub

    Sub cargar_datos_paciente(ByVal numero_asistencia As Integer)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT dni, telefono, nombre, apellidos,domicilio,poblacion FROM 
                                            clientes INNER JOIN asistencias ON  clientes.dni = asistencias.cliente AND asistencias.numero =" + numero_asistencia.ToString)

        For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos
            Select Case i
                Case 0
                    TextBox5.Text = tabla_datos.Rows(0)(i).ToString
                Case 1
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString
                Case 2
                    TextBox2.Text = tabla_datos.Rows(0)(i).ToString
                Case 3
                    TextBox3.Text = tabla_datos.Rows(0)(i).ToString
                Case 5
                    TextBox4.Text = tabla_datos.Rows(0)(i - 1).ToString + ", " + tabla_datos.Rows(0)(i).ToString
            End Select
        Next
    End Sub


    Sub cargar_datos_asistencia(ByVal numero_asistencia As Integer)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT numero, estado, fecha,hora,tipo FROM 
                                            asistencias WHERE numero=" + numero_asistencia.ToString)

        For i As Integer = 0 To tabla_datos.Columns.Count - 1
            Select Case i
                Case 0
                    TextBox9.Text = tabla_datos.Rows(0)(i).ToString
                Case 1
                    ComboBox1.SelectedIndex = CInt(tabla_datos.Rows(0)(i))
                Case 2
                    Dim dia, mes, anio As String
                    anio = tabla_datos.Rows(0)(i).ToString.Substring(0, 4)
                    mes = tabla_datos.Rows(0)(i).ToString.Substring(4, 2)
                    dia = tabla_datos.Rows(0)(i).ToString.Substring(6, 2)
                    MaskedTextBox1.Text = dia + mes + anio
                Case 3
                    MaskedTextBox2.Text = tabla_datos.Rows(0)(i).ToString
                Case 4
                    If CInt(tabla_datos.Rows(0)(i)) > 0 Then
                        ComboBox3.SelectedIndex = CInt(tabla_datos.Rows(0)(i)) - 1
                    Else
                        ComboBox3.SelectedIndex = CInt(tabla_datos.Rows(0)(i))
                    End If

            End Select
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            If ComboBox3.SelectedIndex > 0 Then
                bbdd.actualizarAsistencia(CInt(TextBox9.Text), ComboBox1.SelectedIndex, ComboBox3.SelectedIndex + 1)
            Else
                bbdd.actualizarAsistencia(CInt(TextBox9.Text), ComboBox1.SelectedIndex, ComboBox3.SelectedIndex)
            End If
            MsgBox("Datos guardado satisfactoriamente.", MsgBoxStyle.DefaultButton1, "Actualización")
        Catch ex As Exception
            MsgBox("Error al guardar los datos.", MsgBoxStyle.Critical, "Actualización")
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim dni_medico_deguardia As String
            dni_medico_deguardia = bbdd.devolverTabla("SELECT medicos.dni FROM medicos INNER JOIN guardias ON medicos.dni = guardias.dni AND guardias.fecha= " + formatear_fecha(diaHoy, mesHoy, anioHoy) + " AND (guardias.hora_inicio<CURTIME() AND guardias.hora_fin>CURTIME())").Rows(0)(0).ToString
            Debug.Print("DNI MÉDICO GUARDIA: " + dni_medico_deguardia)
            bbdd.asignarMedicoAsistencia(dni_medico_deguardia, CInt(TextBox9.Text))
            MsgBox("Asistencia enviada al médico: " + dni_medico_deguardia, MsgBoxStyle.DefaultButton1, "Asistencia")
        Catch ex As Exception
            MsgBox("Error al enviar asistencia. Ningún médico de guardia.", MsgBoxStyle.Critical, "Asistencia")
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Private Function formatear_fecha(ByRef dia As String, ByRef mes As String, ByRef anio As String) As String
        Dim fecha_formateada As String
        If (dia.Length < 2) Then
            dia = "0" + dia
        End If
        If (mes.Length < 2) Then
            mes = "0" + mes
        End If
        fecha_formateada = anio + mes + dia
        Return fecha_formateada
    End Function
End Class