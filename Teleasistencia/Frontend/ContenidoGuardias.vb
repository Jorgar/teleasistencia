﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################




Public Class ContenidoGuardias

    Private dni_medico As String
    Private fecha As String
    Private bbdd As New Datos()
    Private h_i, h_f As String
    Private dia, mes, anio As String
    Private fecha_formateada As String


    Private Sub ContenidoGuardias_Load(sender As Object, e As EventArgs) Handles Me.Load
        refrescar()
    End Sub

    Private Sub PictureBox1_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox1.MouseEnter
        PictureBox1.Image = My.Resources.buscar_over
    End Sub

    Private Sub PictureBox1_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox1.MouseLeave
        PictureBox1.Image = My.Resources.buscar_normal
    End Sub

    Private Sub PictureBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseDown
        PictureBox1.Image = My.Resources.buscar_press
    End Sub

    Private Sub PictureBox1_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseUp
        PictureBox1.Image = My.Resources.buscar_normal
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        bbdd.cargarDataGrid("SELECT TIME_FORMAT(hora_inicio, '%H:%i') as 'Hora inicio', TIME_FORMAT(hora_fin, '%H:%i') as 'Hora fin', dni as Médico,
                            concat(substring(fecha,7,2),'/',substring(fecha,5,2),'/',substring(fecha,1,4)) as Fecha
	                        FROM teleasistencia.guardias WHERE teleasistencia.guardias.dni='" + TextBox5.Text + "' OR (SELECT nombre FROM teleasistencia.medicos 
                            WHERE teleasistencia.guardias.dni = teleasistencia.medicos.dni)='" + TextBox5.Text + "' OR  (SELECT apellidos FROM teleasistencia.medicos WHERE 
                            teleasistencia.guardias.dni = teleasistencia.medicos.dni)='" + TextBox5.Text + "' ORDER BY fecha;", datagridview_guardias)
        If datagridview_guardias.RowCount = 0 Then
            Button2.Visible = False
            Button3.Visible = False
        Else
            Button2.Visible = True
            Button3.Visible = True
        End If
    End Sub

    Private Sub PictureBox2_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox2.MouseEnter
        PictureBox2.Image = My.Resources.refreshOver
    End Sub

    Private Sub PictureBox2_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox2.MouseLeave
        PictureBox2.Image = My.Resources.refreshNormal
    End Sub

    Private Sub PictureBox2_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseDown
        PictureBox2.Image = My.Resources.refreshPress
    End Sub

    Private Sub PictureBox2_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox2.MouseUp
        PictureBox2.Image = My.Resources.refreshNormal
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        refrescar()
        If datagridview_guardias.RowCount = 0 Then
            Button2.Visible = False
            Button3.Visible = False
        Else
            Button2.Visible = True
            Button3.Visible = True
        End If
    End Sub


    Private Sub TextBox5_Click(sender As Object, e As EventArgs) Handles TextBox5.Click
        TextBox5.Text = ""
        TextBox5.ForeColor = Color.DimGray
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim n_guardia As New NuevaGuardia()
        n_guardia.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (datagridview_guardias.SelectedCells.Count > 0) Then
            dni_medico = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(2).Value, String)
            fecha = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(3).Value, String)
            h_i = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(0).Value, String)
            h_f = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(1).Value, String)
        End If

        Dim editar_guardia As New EditarGuardia(dni_medico, fecha, h_i, h_f)
        editar_guardia.ShowDialog()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged
        dia = DateTimePicker1.Value.Day.ToString
        mes = DateTimePicker1.Value.Month.ToString
        anio = DateTimePicker1.Value.Year.ToString
        formatear_fecha(dia, mes, anio)
        bbdd.cargarDataGrid("SELECT TIME_FORMAT(hora_inicio, '%H:%i') as 'Hora inicio',TIME_FORMAT(hora_fin, '%H:%i') as 'Hora fin',
                            dni as Médico, concat(substring(fecha,7,2),'/',substring(fecha,5,2),'/',substring(fecha,1,4)) as Fecha
	                        FROM teleasistencia.guardias WHERE fecha='" + fecha_formateada + "' ORDER BY hora_inicio;", datagridview_guardias)
        If datagridview_guardias.RowCount = 0 Then
            Button2.Visible = False
            Button3.Visible = False
        Else
            Button2.Visible = True
            Button3.Visible = True
        End If
    End Sub

    'GETTERS Y SETTERS
    Public Property getSetFecha() As String
        Get
            Return fecha
        End Get
        Set(ByVal value As String)
            fecha = value
        End Set
    End Property

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea eliminar la guardia?", MsgBoxStyle.YesNo, "Eliminar guardia")
        If comprobacion = 6 Then
            If (datagridview_guardias.SelectedCells.Count > 0) Then
                dni_medico = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(2).Value, String)
                fecha = CType(datagridview_guardias.Rows(datagridview_guardias.CurrentRow.Index).Cells(3).Value, String)
                formatear_fecha(fecha.Substring(0, 2), fecha.Substring(3, 2), fecha.Substring(6, 4))
                bbdd.eliminarGuardia(dni_medico, fecha_formateada)
            End If
        End If
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged

    End Sub

    Public Property getSetDniMedico() As String
        Get
            Return dni_medico
        End Get
        Set(ByVal value As String)
            dni_medico = value
        End Set
    End Property

    Private Sub formatear_fecha(ByRef dia As String, ByRef mes As String, ByRef anio As String)
        If (dia.Length < 2) Then
            dia = "0" + dia
        End If
        If (mes.Length < 2) Then
            mes = "0" + mes
        End If
        fecha_formateada = anio + mes + dia
    End Sub

    Public Sub refrescar()
        dia = DateTimePicker1.Value.Day.ToString
        mes = DateTimePicker1.Value.Month.ToString
        anio = DateTimePicker1.Value.Year.ToString
        formatear_fecha(dia, mes, anio)
        bbdd.cargarDataGrid("SELECT TIME_FORMAT(hora_inicio, '%H:%i') as 'Hora inicio',TIME_FORMAT(hora_fin, '%H:%i') as 'Hora fin',
                            dni as Médico, concat(substring(fecha,7,2),'/',substring(fecha,5,2),'/',substring(fecha,1,4)) as Fecha
	                        FROM teleasistencia.guardias WHERE fecha=" + fecha_formateada + " ORDER BY hora_inicio;", datagridview_guardias)
        If datagridview_guardias.RowCount = 0 Then
            Button2.Visible = False
            Button3.Visible = False
        Else
            Button2.Visible = True
            Button3.Visible = True
        End If
    End Sub

End Class