﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################









Public Class ContenidoAsistencias

    'variables
    Private bbdd As New Datos()
    Private dni_medico_logueado As String = Informacion.login
    Private dni_cliente As String

    Private Sub ContenidoAsistencias_Load(sender As Object, e As EventArgs) Handles Me.Load
        rellenar_estado()
        rellenar_tipo()
        bbdd.cargarComboBox("SELECT dni FROM medicos", ComboBox1)
        bbdd.cargarListBox("SELECT numero FROM asistencias WHERE atiende='" + dni_medico_logueado + "' AND ((SUBSTRING(fecha,1,4)=YEAR(CURRENT_DATE()) AND SUBSTRING(fecha, 5, 2)=MONTH(CURRENT_DATE())) 
                              OR (tipo=1 OR tipo=0))  ORDER BY numero DESC", ListBox1, True) 'cargamos el Listbox1 con las asistencias del médico logeado
        If ListBox1.Items.Count > 0 Then
            ListBox1.SelectedIndex = 0
        End If
    End Sub

    'Método que rellena el combobox de 
    'estado de asistencia/urgencia a partir
    'de su enumaracion.
    Sub rellenar_estado()
        For Each item In System.Enum.GetValues(GetType(Informacion.estado_alarma))
            ComboBox2.Items.Add(Informacion.getDescripcion(CType(item, [Enum])))
        Next
    End Sub

    'Método que rellena el combobox de 
    'tipo de asistencia/urgencia a partir
    'de su enumaracion.
    Sub rellenar_tipo()
        For Each item In System.Enum.GetValues(GetType(Informacion.tipo_urgencia))
            ComboBox3.Items.Add(Informacion.getDescripcion(CType(item, [Enum])))
        Next
    End Sub


    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        cargar_datos_paciente(CInt(ListBox1.SelectedItem.ToString))
        cargar_datos_asistencia(CInt(ListBox1.SelectedItem.ToString))
    End Sub

    Sub cargar_datos_paciente(ByVal numero_asistencia As Integer)
        Panel18.Visible = True
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT nombre, apellidos, dni, telefono,sexo,fecha_nacimiento,domicilio,poblacion FROM 
                                            clientes INNER JOIN asistencias ON clientes.dni = asistencias.cliente AND asistencias.numero =" + numero_asistencia.ToString)

        For i As Integer = 0 To tabla_datos.Columns.Count - 1   'recorremos el datatable y asignamos datos a los objetos
            Select Case i
                Case 0  'nombre
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString
                Case 1  'apellidos
                    TextBox6.Text = tabla_datos.Rows(0)(i).ToString + ", " + tabla_datos.Rows(0)(i - 1).ToString
                Case 2  'dni
                    TextBox10.Text = "DNI: " + tabla_datos.Rows(0)(i).ToString
                    dni_cliente = tabla_datos.Rows(0)(i).ToString
                Case 3  'telefono
                    TextBox11.Text = "Tel: " + tabla_datos.Rows(0)(i).ToString
                Case 4  'sexo
                    If (tabla_datos.Rows(0)(i).ToString.Equals("H")) Then
                        TextBox12.Text = "HOMBRE"
                    Else
                        TextBox12.Text = "MUJER"
                    End If

                Case 5  'nacimiento
                    Dim dia, mes, anio As String
                    anio = tabla_datos.Rows(0)(i).ToString.Substring(0, 4)
                    mes = tabla_datos.Rows(0)(i).ToString.Substring(4, 2)
                    dia = tabla_datos.Rows(0)(i).ToString.Substring(6, 2)
                    MaskedTextBox5.Text = dia + mes + anio
                Case 6  'domicilio
                    TextBox14.Text = tabla_datos.Rows(0)(i).ToString
                Case 7  'poblacion
                    TextBox14.Text += ", " + tabla_datos.Rows(0)(i).ToString
            End Select
        Next
        Try
            Dim patologia_prin As String = bbdd.devolverTabla("SELECT patologias.descripcion FROM patologias INNER JOIN patologias_clientes ON patologias.codigo = patologias_clientes.patologia 
                                    AND patologias_clientes.descripcion = 'PRINCIPAL' AND patologias_clientes.dni_cliente = '" + dni_cliente + "'").Rows(0)(0).ToString
            TextBox2.Text = "Patología principal: " + Char.ToUpper(patologia_prin(0)) & patologia_prin.Substring(1)
        Catch ex As Exception
            TextBox2.Text = "Patología principal: ns/nc"
            Debug.Print(ex.ToString)
        End Try
        'Primera letra en mayúscula
    End Sub

    Sub cargar_datos_asistencia(ByVal numero_asistencia As Integer)
        Dim tabla_datos As Data.DataTable
        tabla_datos = bbdd.devolverTabla("SELECT numero, estado, fecha,hora,tipo,atiende,observaciones,cliente,finalizada FROM 
                                            asistencias WHERE numero=" + numero_asistencia.ToString)

        For i As Integer = 0 To tabla_datos.Columns.Count - 1
            Select Case i
                Case 0  'numero
                    TextBox5.Text = tabla_datos.Rows(0)(i).ToString
                Case 1  'estado
                    ComboBox2.SelectedIndex = CInt(tabla_datos.Rows(0)(i))
                Case 2  'fecha
                    Dim dia, mes, anio As String
                    anio = tabla_datos.Rows(0)(i).ToString.Substring(0, 4)
                    mes = tabla_datos.Rows(0)(i).ToString.Substring(4, 2)
                    dia = tabla_datos.Rows(0)(i).ToString.Substring(6, 2)
                    MaskedTextBox1.Text = dia + mes + anio
                Case 3  'hora
                    MaskedTextBox2.Text = tabla_datos.Rows(0)(i).ToString
                Case 4  'tipo
                    ComboBox3.SelectedIndex = CInt(tabla_datos.Rows(0)(i))
                Case 5  'atiende
                    ComboBox1.Text = tabla_datos.Rows(0)(i).ToString
                Case 6  'observaciones
                    RichTextBox1.Text = tabla_datos.Rows(0)(i).ToString
                Case 7  'cliente
                    TextBox1.Text = tabla_datos.Rows(0)(i).ToString
                Case 8  'finalizada
                    MaskedTextBox3.Text = tabla_datos.Rows(0)(i).ToString
            End Select
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If Not MaskedTextBox3.MaskCompleted Then
                bbdd.actualizarAsistencia(CInt(TextBox5.Text), Nothing, RichTextBox1.Text, ComboBox2.SelectedIndex)
            Else
                bbdd.actualizarAsistencia(CInt(TextBox5.Text), MaskedTextBox3.Text, RichTextBox1.Text, ComboBox2.SelectedIndex)
            End If

            MsgBox("Asistencia enviada actualizada satisfactoriamente", MsgBoxStyle.DefaultButton1, "Asistencia")
        Catch ex As Exception
            MsgBox("Error al enviar asistencia.", MsgBoxStyle.Critical, "Asistencia")
            Debug.Print(ex.ToString)
        End Try
    End Sub
End Class