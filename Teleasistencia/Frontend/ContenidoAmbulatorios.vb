﻿'##########################################################################################
'#
'#      © Copyright 2017 Jordi Garcia Sapiña
'#
'#      This file Is part Of Teleasistencia.
'# 
'#      Teleasistencia Is free software: you can redistribute it And/Or modify
'#      it under the terms Of the GNU General Public License As published by
'#      the Free Software Foundation, either version 3 Of the License, Or
'#      (at your option) any later version.
'#
'#      Teleasistencia Is distributed In the hope that it will be useful,
'#      but WITHOUT ANY WARRANTY; without even the implied warranty of
'#      MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'#      GNU General Public License For more details.
'#      
'#      You should have received a copy Of the GNU General Public License
'#      along with Teleasistencia.  If Not, see < http: //www.gnu.org/licenses/>.
'#
'##########################################################################################



Public Class ContenidoAmbulatorios

    'Variables
    Dim bbdd As New Datos()

    Private Sub ContenidoHospitales_Load(sender As Object, e As EventArgs) Handles Me.Load
        bbdd.tablasDataGridView(datagridview1, "SELECT * FROM ambulatorios")
        CType(Me.datagridview1.Columns(0), DataGridViewTextBoxColumn).MaxInputLength = 8
        CType(Me.datagridview1.Columns(1), DataGridViewTextBoxColumn).MaxInputLength = 25
        CType(Me.datagridview1.Columns(2), DataGridViewTextBoxColumn).MaxInputLength = 30
        CType(Me.datagridview1.Columns(3), DataGridViewTextBoxColumn).MaxInputLength = 30
        CType(Me.datagridview1.Columns(4), DataGridViewTextBoxColumn).MaxInputLength = 5
        CType(Me.datagridview1.Columns(5), DataGridViewTextBoxColumn).MaxInputLength = 9
        CType(Me.datagridview1.Columns(6), DataGridViewTextBoxColumn).MaxInputLength = 9
        CType(Me.datagridview1.Columns(7), DataGridViewTextBoxColumn).MaxInputLength = 50
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            bbdd.actualizarDataGrid()
            MsgBox("Datos guardados satisfactoriamente", MsgBoxStyle.DefaultButton1, "Datos guardados")
        Catch ex As Exception
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim comprobacion As Integer = MsgBox("¿Está usted seguro de que desea cancelar la edición?", MsgBoxStyle.YesNo, "Cancelar edición")
        If comprobacion = 6 Then
            bbdd.tablasDataGridView(datagridview1, "SELECT * FROM ambulatorios")
        End If
    End Sub

    'Filtrado keypress

    Private Sub datagridview1_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles datagridview1.EditingControlShowing

        RemoveHandler CType(e.Control, TextBox).KeyPress, AddressOf filtrado_numeros

        Dim columna As Integer = datagridview1.CurrentCell.ColumnIndex
        If columna = 0 Or columna = 4 Or columna = 5 Or columna = 6 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf filtrado_numeros
        End If
    End Sub

    Private Sub filtrado_numeros(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Select Case e.KeyChar.IsNumber(e.KeyChar)
            Case True
                'Aceptamos la tecla
            Case Else
                Select Case e.KeyChar
                    Case ControlChars.Cr
                        'System.Windows.Forms.SendKeys.Send(Chr(Keys.Tab))
                    Case ControlChars.Back
                    Case Chr(Keys.Space)
                    Case Else
                        e.Handled = True 'Anulamos la tecla
                End Select
        End Select
    End Sub

End Class