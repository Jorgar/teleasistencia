# TELEASISTENCIA #
_________________________________________________________

Software de escritorio desarrollado como proyecto final para la asignatura* Desarrollo de Interfaces* en el Grado Superior *Desarrollo de Aplicaciones Multiplataforma* (DAM). Software de gestión administrativa de una empresa ficticia de teleasistencia para casos de urgencia a pacientes con movilidad reducida y de avanzada edad. Desarrollado en *Visual Studio 2015 Community* y *MySQL Workbench 6.3.* Diseños en *Clip Paint Studio EX, Adobe Photoshop CC (versión de prueba gratuíta) y Adobe Illustrator CC (versión de prueba gratuíta)*

### Licencia ###

Teleasistencia está publicado bajo la licencia [General Public License (GNU)](https://www.gnu.org/licenses/gpl-3.0.en.html)


![GPLv3_Logo.svg.png](https://bitbucket.org/repo/X8dza7/images/2333740753-GPLv3_Logo.svg.png)



*Owner Repository: Jorgar (Jordi García)*

*Contact: jordigarciadev@gmail.com*


### Capturas ###

![2017-03-13_23h15_23.png](https://bitbucket.org/repo/X8dza7/images/182948362-2017-03-13_23h15_23.png)

![2017-03-13_23h16_48.png](https://bitbucket.org/repo/X8dza7/images/3727498722-2017-03-13_23h16_48.png)

![2017-03-13_23h27_20.png](https://bitbucket.org/repo/X8dza7/images/593261150-2017-03-13_23h27_20.png)

![2017-03-13_23h17_13.png](https://bitbucket.org/repo/X8dza7/images/3726694766-2017-03-13_23h17_13.png)

![2017-03-13_23h09_21.png](https://bitbucket.org/repo/X8dza7/images/3848365097-2017-03-13_23h09_21.png) 


### Base de datos ###

*Descargar la base de datos MySQL: [Teleasistencia.sql](https://bitbucket.org/Jorgar/teleasistencia/downloads/teleasistencia.sql)* 

*Descargar notas adjuntas sobre la bbdd: [notas.txt](https://bitbucket.org/Jorgar/teleasistencia/downloads/NOTA.txt)*

![diagrama.png](https://bitbucket.org/repo/X8dza7/images/1182904834-diagrama.png)
*Bosquejo del diagrama entidad-relación*